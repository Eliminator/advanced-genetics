package io.hotmail.com.jacob_vejvoda.advanced_genetics;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
//import net.minecraft.server.v1_9_R2.NBTTagCompound;
//import net.minecraft.server.v1_9_R2.NBTTagList;
//import net.minecraft.server.v1_9_R2.TileEntityDispenser;
//import net.minecraft.server.v1_9_R2.TileEntityFurnace;
//
//import org.bukkit.craftbukkit.v1_9_R2.block.CraftDispenser;
//import org.bukkit.craftbukkit.v1_9_R2.block.CraftFurnace;
//import org.bukkit.craftbukkit.v1_9_R2.inventory.CraftItemStack;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.Dispenser;
import org.bukkit.block.Furnace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Damageable;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
//import org.bukkit.entity.Skeleton.SkeletonType;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDispenseEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityTargetLivingEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
//import org.bukkit.material.Button;
//import org.bukkit.material.Lever;
import org.bukkit.plugin.java.JavaPlugin;
//import org.bukkit.potion.Potion;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.BlockIterator;
import org.bukkit.util.Vector;

public class advanced_genetics extends JavaPlugin implements Listener{
		private boolean stopped = false;
		private FileConfiguration saves = null;
		private File savesFile = null;
		ArrayList<String> mobList = new ArrayList<String>();
		ArrayList<String> dnaTypesList = new ArrayList<String>();
		ArrayList<String> virusList = new ArrayList<String>();
		ArrayList<AbilityUser> abilityUserList = new ArrayList<AbilityUser>();
		ArrayList<machineBlock> machineBlockList = new ArrayList<machineBlock>();
		public static ArrayList<Integer> transBlocks = new ArrayList<Integer>();
		public static ArrayList<Integer> noVineBlocks = new ArrayList<Integer>();
		public static ArrayList<String> climbingPlayers = new ArrayList<String>();
		Map<String, ArrayList<Block>> vineMap = new HashMap<String, ArrayList<Block>>();
		ArrayList<String> villagerWait = new ArrayList<String>();
		HashMap<Player, Double> lastYMap = new HashMap<Player, Double>();
		ArrayList<String> stoneList = new ArrayList<String>();
		ArrayList<String> slimeList = new ArrayList<String>();
		//HashMap<Inventory, Inventory> invMap = new HashMap<Inventory, Inventory>();
		
		@Override
	    public void onEnable(){
	    	getServer().getPluginManager().registerEvents(this, this);
	    	if (!new File(getDataFolder(), "config.yml").exists()) {
	    	     saveDefaultConfig();
	    	}	
	    	if (!new File(getDataFolder(), "saves.yml").exists()) {
	    	     saveDefaultSaves();
	    	}	
			try {
			    Metrics metrics = new Metrics(this);
			    metrics.start();
			} catch (IOException e) {
			    // Failed to submit the stats :-(
			}
    		addMobs(mobList);
	    	defineTransBlocks();
    		addRecipes();
    		addVirusTypes(virusList);
    		addDnaTypes(dnaTypesList);
	    	ArrayList<World> started = new ArrayList<World>();
	    	for(Player p : getServer().getOnlinePlayers()){
	    		//join(p);
	    		if(started.contains(p.getWorld()) == false){
	    			startParticles(p.getWorld());
	    			started.add(p.getWorld());
	    		}
	    	}
    		timer();
    		//fastTimer();
	    }
	    
	    public void onDisable(){
	    	stopped = true;
	    }
	    
	    @SuppressWarnings("deprecation")
		public void reloadSaves() {
	        if (savesFile == null) {
	        	savesFile = new File(getDataFolder(), "saves.yml");
	        }
	        saves = YamlConfiguration.loadConfiguration(savesFile);
	     
	        // Look for defaults in the jar
	        InputStream defConfigStream = this.getResource("saves.yml");
	        if (defConfigStream != null) {
	            YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
	            saves.setDefaults(defConfig);
	        }
	    }
	    
	    public FileConfiguration getSaves() {
	        if (saves == null) {
	        	reloadSaves();
	        }
	        return saves;
	    }
	    
	    public void saveSaves() {
	        if (saves == null || savesFile == null) {
	            return;
	        }
	        try {
	            getSaves().save(savesFile);
	        } catch (IOException ex) {
	            getLogger().log(Level.SEVERE, "Could not save config to " + savesFile, ex);
	        }
	    }
	    
	    public void saveDefaultSaves() {
	        if (savesFile == null) {
	        	savesFile = new File(getDataFolder(), "saves.yml");
	        }
	        if (!savesFile.exists()) {            
	             //plugin.saveResource("saves.yml", false);
	        	try {
					new File(getDataFolder(), "saves.yml").createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
	         }
	    }
	    
		@SuppressWarnings("unchecked")
		public void addSave(World world, Location location, String saveSpot) {
	    	double x = location.getX();
	    	double y = location.getY();
	    	double z = location.getZ();
	    	String name = getCoordName(location);
			if (getSaves().getString(saveSpot + "." + world.getName() + "." + name) == null) {
			    getSaves().set(saveSpot + "." + world.getName() + "." + name + ".x", x);
			    getSaves().set(saveSpot + "." + world.getName() + "." + name + ".y", y);
			    getSaves().set(saveSpot + "." + world.getName() + "." + name + ".z", z);
			    ArrayList<String> stringList = new ArrayList<String>();
			    if (getSaves().getList("saveNameList") != null){
			    	stringList = (ArrayList<String>) getSaves().getList("saveNameList");
			    }
			    stringList.add(name);
			    getSaves().set("saveNameList", stringList);
	    		saveSaves();
	    	}
		}
		
		@SuppressWarnings("unchecked")
		public void removeSave(World world, Location location, String saveSpot) {
	    	double x = location.getX();
	    	double y = location.getY();
	    	double z = location.getZ();
	    	String name = (x + "." + y + "." + z);
		    name = name.replace(".", "");
			if (getSaves().getString(saveSpot + "." + world.getName() + "." + name) != null) {
			    getSaves().set(saveSpot + "." + world.getName() + "." + name, null);
			    ArrayList<String> stringList = (ArrayList<String>) getSaves().getList("saveNameList");
			    stringList.remove(name);
			    getSaves().set("saveNameList", stringList);
	    		saveSaves();
	    	}
		}
		
		public boolean isSave(World world, Location location, String saveSpot) {
	    	double x = location.getX();
	    	double y = location.getY();
	    	double z = location.getZ();
	    	String name = (x + "." + y + "." + z);
		    name = name.replace(".", "");
			if (getSaves().getString(saveSpot + "." + world.getName() + "." + name) != null) {
	    		return true;
	    	}
			return false;
		}
		
//		@SuppressWarnings({ "deprecation", "unchecked" })
//		public void fastTimer(){
//			if(stopped)
//				return;
//			for (Entry<Inventory, Inventory> cp : inv.entrySet()){
//				
//			}
//			//Tick
//			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
//				     public void run() {
//				    	 try{
//				    		 fastTimer();
//				    	 }catch(Exception x){x.printStackTrace();}
//					 }
//			},(1 * 20));
//		}
		
		@SuppressWarnings({ "unchecked" })
		public void timer(){
			if(stopped)
				return;
			//Virus'
			for(Player p : getServer().getOnlinePlayers())
				if ((getConfig().getList("enabledworlds").contains(p.getWorld().getName())) || (getConfig().getList("enabledworlds").contains("<all>")))
					if(getSaves().getList("playerList." + p.getName()) != null){
						ArrayList<String> sickList = new ArrayList<String>();
						for(String dna : (ArrayList<String>)getSaves().getList("playerList." + p.getName()))
							if(virusList.contains(dna))
								sickList.add(dna);
						if(!sickList.isEmpty())
							virusEffect(p, sickList);
					}
			//Melt Check
			
			//Tick
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				     public void run() {
				    	 try{
				    		 timer();
				    	 }catch(Exception x){x.printStackTrace();}
					 }
			},(10 * 20));
		}
		
		public void virusEffect(LivingEntity p, ArrayList<String> sickList){
			int slowLvl = 0;
			int breakLvl = 0;
			int weakLvl = 0;
			int nauseaLvl = 0;
			int blindLvl = 0;
			int wtiherLvl = 0;
			int hungerLvl = 0;
			int strengthLvl = 0;
			int poisonLvl = 0;
			int nVisionLvl = 0;
			int fireLvl = 0;
			if(sickList.contains("Flu")){
				slowLvl = slowLvl + 1;
				breakLvl = breakLvl + 1;
				weakLvl = weakLvl + 1;
				nauseaLvl = nauseaLvl + 1;
			}
			if(sickList.contains("Black Death")){
				wtiherLvl = wtiherLvl + 1;
				hungerLvl = hungerLvl + 1;
				weakLvl = weakLvl + 2;
			}
			if(sickList.contains("Ebola")){
				slowLvl = slowLvl + 2;
				breakLvl = breakLvl + 5;
				strengthLvl = strengthLvl + 5;
			}
			if(sickList.contains("Anthrax")){
				blindLvl = blindLvl + 1;
				hungerLvl = hungerLvl + 3;
				weakLvl = weakLvl + 1;
				nauseaLvl = nauseaLvl + 2;
			}
			if(sickList.contains("Scarlet Fever")){
				slowLvl = slowLvl + 3;
				weakLvl = weakLvl + 5;
				nauseaLvl = nauseaLvl + 3;
				fireLvl = fireLvl + 1;
			}
			if(sickList.contains("SmallPox")){
				nVisionLvl = nVisionLvl + 1;
				blindLvl = blindLvl + 1;
				slowLvl = slowLvl + 1;
				weakLvl = weakLvl + 10;
				poisonLvl = poisonLvl + 1;
			}
			if(slowLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, (20*20), slowLvl));
			if(breakLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, (20*20), breakLvl));
			if(weakLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, (20*20), weakLvl));
			if(nauseaLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, (20*20), nauseaLvl));
			if(blindLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, (20*20), blindLvl));
			if(wtiherLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 3*20, wtiherLvl));
			if(hungerLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.HUNGER, (20*20), hungerLvl));
			if(strengthLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, (15*20), strengthLvl));
			if(poisonLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.POISON, 3*20, poisonLvl));
			if(nVisionLvl > 0)
				 p.addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, (20*20), nVisionLvl));
			if(fireLvl > 0)
				 p.setFireTicks(3);
			//Catch Check
			int min = 1;
			int max = 10;
			int rn = new Random().nextInt(max - min + 1) + min;
			for(Entity e : p.getNearbyEntities(7,7,7)){
				if((sickList.contains("Anthrax")) && (rn == 10))
					if(e instanceof Player){
						addVirus(((Player)e), "Anthrax");
					}else if((e instanceof LivingEntity) && (e instanceof Player == false))
						virusEffect(((LivingEntity)e), new ArrayList<String>(Arrays.asList("Anthrax")));
				if((sickList.contains("Flu")) && (rn <= 3))
					if(e instanceof Player)
						addVirus(((Player)e), "Flu");
				if((sickList.contains("Black Death")) && (rn == 4))
					if(e instanceof Player)
						addVirus(((Player)e), "Black Death");
				if((sickList.contains("Scarlet Fever")) && (rn == 5))
					if(e instanceof Player)
						addVirus(((Player)e), "Scarlet Fever");
				if((sickList.contains("SmallPox")) && (rn >= 9))
					if(e instanceof Player)
						addVirus(((Player)e), "SmallPox");
			}
			//Get Better Chance
			int min2 = 1;
			int max2 = 100;
			int rn2 = new Random().nextInt(max2 - min2 + 1) + min2;
			if(p instanceof Player){
				if((rn2 <= 10) && (sickList.contains("Flu")))
					removeVirus((Player)p, "Flu");
				if((rn2 >= 96) && (sickList.contains("Anthrax")))
					removeVirus((Player)p, "Anthrax");
				if((rn2 <= 30) && (rn2 >= 22) && (sickList.contains("Black Death")))
					removeVirus((Player)p, "Black Death");
				if((rn2 <= 50) && (rn2 >= 44) && (sickList.contains("Scarlet Fever")))
					removeVirus((Player)p, "Scarlet Fever");
				if((rn2 <= 70) && (rn2 >= 66) && (sickList.contains("SmallPox")))
					removeVirus((Player)p, "SmallPox");
				if((rn2 == 12) && (sickList.contains("Ebola")))
					removeVirus((Player)p, "Ebola");
			}
		}
		
		@SuppressWarnings("unchecked")
		@EventHandler(priority=EventPriority.HIGH)
		public void onMobAggro(EntityTargetLivingEntityEvent event){
			try{
				Entity target = event.getTarget();
				World world = target.getWorld();
				Entity ent = event.getEntity();
				if((ent instanceof Zombie) || (ent instanceof Skeleton) || (ent instanceof Creeper))
					if(target instanceof Player){
						Player player = (Player) target;
						if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
							ArrayList<String> abilityList = new ArrayList<String>();
					    	if(getSaves().getList("playerList." + player.getName()) != null){
					    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
					    	}
							if((abilityList.contains("Undead")) && (getConfig().getBoolean("Undead") == true)){
									event.setCancelled(true);
							}
						}
					}
			}catch(Exception x){}
		}
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		@EventHandler(priority=EventPriority.HIGH)
	    public void onPlayerMove(PlayerMoveEvent event){
			final Player player = event.getPlayer();
			World world = player.getWorld();
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
				Location underneath = player.getLocation();
				underneath.setY(underneath.getY()-1);
				final Block uBlock = underneath.getBlock();
				ArrayList<String> abilityList = new ArrayList<String>();
		    	if(getSaves().getList("playerList." + player.getName()) != null){
		    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
		    	}
		    	//Stone
//		    	if(stoneList.contains(player.getName())){
//		    		Location l = lastLoc.get(player);
//		    		if((l.getBlockX() != player.getLocation().getX()) || (l.getBlockY() != player.getLocation().getY()) ||(l.getBlockZ() != player.getLocation().getZ())){
//		    			Location nLoc = player.getLocation().clone();
//		    			nLoc.setX(l.getX());
//		    			nLoc.setY(l.getY());
//		    			nLoc.setZ(l.getZ());
//		    			player.teleport(nLoc);
//		    		}
//		    	}
				if((abilityList.contains("Flying")) && (getConfig().getBoolean("Flying") == true)){
//				    if((uBlock.getType().equals(Material.AIR)) && (player.getAllowFlight() == false)){
//				    	player.setAllowFlight(true);
//				    }else if ((!uBlock.getType().equals(Material.AIR)) && (player.getAllowFlight() == true)){
//				    	player.setAllowFlight(false);
//				    }
					if(player.getAllowFlight() == false)
						player.setAllowFlight(true);
					if(((abilityList.contains("NoFall") == false) || (getConfig().getBoolean("NoFall") == false))){
						//On Fly
						Location l = player.getLocation().clone();
						l.setY(l.getBlockY()-1);
						Location l2 = l.clone();
						l2.setY(l2.getBlockY()-1);
						if(lastYMap.get(player) == null)
							lastYMap.put(player, player.getLocation().getY());
						double ly = lastYMap.get(player);
						double y = player.getLocation().getY();
						if(y+0.5<ly){
							//System.out.println("Fall1");
							if((!l.getBlock().getType().equals(Material.AIR)) || (!l2.getBlock().getType().equals(Material.AIR))){
								//System.out.println("Fall2");
								//Off fly
								if(player.getAllowFlight() == true)
									player.setAllowFlight(false);
							}
						}
						lastYMap.put(player, player.getLocation().getY());
					}
				}
//				if ((abilityList.contains("NoFall")) && (getConfig().getBoolean("NoFall") == true) && (player.isFlying() == false)){
//					if(uBlock.getType().equals(Material.AIR)){
//						Vector slowedVel = player.getVelocity();
//						slowedVel.setY(slowedVel.getY()/2);
//						player.setVelocity(slowedVel);
//					}
//				}
				if ((abilityList.contains("DayBurn")) && (getConfig().getBoolean("DayBurn") == true)){
					Block highestBlock = player.getWorld().getHighestBlockAt(player.getLocation());
					if((!uBlock.getType().equals(Material.AIR)) && (uBlock.getLightLevel() >= 10 )){
						if((highestBlock.getLocation().getY() < (player.getLocation().getY()+1)) || highestBlock.getType().equals(Material.IRON_FENCE) || highestBlock.getType().equals(Material.NETHER_FENCE) || highestBlock.getType().equals(Material.FENCE_GATE) || highestBlock.getType().equals(Material.LONG_GRASS) || highestBlock.getType().equals(Material.FENCE) || highestBlock.getType().equals(Material.TORCH) || highestBlock.getType().equals(Material.WEB) || highestBlock.getType().equals(Material.GLASS) || highestBlock.getType().equals(Material.STAINED_GLASS)){
							player.setFireTicks(5 * 20);
						}
					}
				}
				if ((abilityList.contains("Jump")) && (getConfig().getBoolean("Jump") == true)){
					Block block, control;
					Vector dir = event.getPlayer().getVelocity().setY(event.getPlayer().getVelocity().getY()*getConfig().getInt("jumpHight"));
					if(event.getTo().getY() > event.getFrom().getY()){
						block = event.getPlayer().getWorld().getBlockAt(new Location(event.getPlayer().getWorld(), event.getTo().getX(), event.getTo().getY()+2, event.getTo().getZ()));
						control = event.getPlayer().getWorld().getBlockAt(new Location(event.getPlayer().getWorld(), event.getTo().getX(), event.getTo().getY()-2, event.getTo().getZ()));
					    if(!(block.getTypeId() != 0 || control.getTypeId() == 0)){
							event.getPlayer().setVelocity(dir);
						}
					}
				}
			    // -- Climbing -- Spider --
				if ((abilityList.contains("Climb")) && (getConfig().getBoolean("Climb") == true)){
			        BlockFace bf = yawToFace(player.getLocation().getYaw());
			        Block block = player.getLocation().getBlock().getRelative(bf);
	
			        if (block.getType() != Material.AIR) {
			          for (int i = 0; i < 300; i++) {
			            Block temp = block.getLocation().add(0.0D, i, 0.0D).getBlock();
			            Block opp = player.getLocation().add(0.0D, i, 0.0D).getBlock();
			            Block aboveOpp = opp.getLocation().add(0.0D, 1.0D, 0.0D).getBlock();
			            int counter = 0;
			            for (int k = 0; k < noVineBlocks.size(); k++) {
			              if ((temp.getType() != Material.AIR) && (temp.getTypeId() != noVineBlocks.get(k).intValue()))
			                counter++;
			            }
			            try {
			            if ((counter != noVineBlocks.size()) || ((opp.getType() != Material.AIR))) break;
			            }finally{}
			            if (aboveOpp.getType() == Material.AIR) {
			              player.sendBlockChange(opp.getLocation(), Material.VINE, (byte)0);
			              addVines(player, opp);
			            }
			            player.setFallDistance(0.0F);
			          }
	
			        }
			        else
			        {
			          for (int i = 0; i < getVines(player).size(); i++) {
			            player.sendBlockChange(((Block)getVines(player).get(i)).getLocation(), Material.AIR, (byte)0);
			          }
			          getVines(player).clear();
			        }
			        
			        Location tophead = player.getEyeLocation();
			        tophead.setY(tophead.getY() +1);
			        
//			        if (!tophead.getBlock().getType().equals(Material.AIR)) {
//			        	player.setAllowFlight(true);
//			        	player.setFlying(true);
//			        	//System.out.println("Flying!");
//			        }else if (tophead.getBlock().getType().equals(Material.AIR) && (player.isFlying())) {
//			        	player.setFlying(false);
//			        	player.setAllowFlight(false);
//			        	//System.out.println("Falling!");
//			        }
			    }	
			}
		}

		@EventHandler(priority=EventPriority.HIGH)
		public void onBlockBreak(BlockBreakEvent event){
            Block block = event.getBlock();
            if(getDispenser(block) != null){
            	Dispenser dispenser = getDispenser(block);
				if ((isExtractor(dispenser.getBlock())) && (dispenser.getInventory() != null) && (dispenser.getInventory().getType().equals(InventoryType.DISPENSER))){
					removeExtractor(dispenser, event.getBlock(),3,7,5);
				}
            }
            if(stoneList.contains(event.getPlayer().getName()))
            	event.setCancelled(true);
            for(Entity e : event.getPlayer().getNearbyEntities(15, 15, 15))
            	if(e instanceof Player)
	            	if(stoneList.contains(((Player)e).getName())){
	            		Location l = e.getLocation().clone();
	            		l.setY(l.getY() - 1);
	            		if(l.getBlock().equals(event.getBlock()))
	            			stopHide(((Player)e));
	            	}
		}
		
		public void removeExtractor(Dispenser d, Block b, int input, int fuel, int output){
			try{
				ItemStack in = d.getInventory().getItem(input).clone();
				ItemStack fu = d.getInventory().getItem(fuel).clone();
				ItemStack ou = d.getInventory().getItem(output).clone();
				if(in != null)
					d.getWorld().dropItemNaturally(d.getLocation(), in);
				if(fu != null)
					d.getWorld().dropItemNaturally(d.getLocation(), fu);
				if(ou != null)
					d.getWorld().dropItemNaturally(d.getLocation(), ou);
				d.getWorld().dropItemNaturally(d.getLocation(), new ItemStack(Material.DISPENSER, 1));
			}catch(Exception x){}
			b.getDrops().clear();
			d.getInventory().clear();
			d.getBlock().setType(Material.AIR);
		}
						
		public Dispenser getDispenser(Block b){
			if(b.getType().equals(Material.DISPENSER)){
				return (Dispenser)b.getState();
			}else if(b.getType().equals(Material.LEVER)){
	            Lever lever = (Lever)b.getState().getData();
	            Block leverOn = b.getRelative(lever.getAttachedFace());	
	            if(leverOn.getType().equals(Material.DISPENSER)){
	            	return (Dispenser)leverOn.getState();
	            }
			}else if(b.getType().equals(Material.STAINED_GLASS)){
				Location d = b.getLocation().clone();
				d.setY(d.getY() - 1);
	            if(d.getBlock().getType().equals(Material.DISPENSER)){
	            	return (Dispenser)d.getBlock().getState();
	            }
			}else if(b.getType().equals(Material.DAYLIGHT_DETECTOR)){
				Location d = b.getLocation().clone();
				d.setY(d.getY() - 2);
	            if(d.getBlock().getType().equals(Material.DISPENSER)){
	            	return (Dispenser)d.getBlock().getState();
	            }
			}
			return null;
		}
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		@EventHandler(priority=EventPriority.HIGH)
		public void onPlayerDamaged(EntityDamageEvent event){
			//System.out.println("Player damaged by " + event.getCause()); //------DEBUG------
			if(event.getEntity() instanceof Player){
				final Player player = (Player) event.getEntity();
				World world = player.getWorld();
				if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
					ArrayList<String> abilityList = new ArrayList<String>();
			    	if(getSaves().getList("playerList." + player.getName()) != null){
			    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
			    	}
					if((abilityList.contains("NoFall")) && (getConfig().getBoolean("NoFall") == true)){
						if(event.getCause().equals(DamageCause.FALL)){
							//System.out.println("Player sgnd!"); //------DEBUG------
							event.setCancelled(true);
						}
					}else if((abilityList.contains("Flying")) && (getConfig().getBoolean("Flying") == true)){
						if(event.getCause().equals(DamageCause.FALL)){
							//double lastY = userGet(player).getLastY;
							//System.out.println("Player's fly speed: " + player.getFlySpeed()); //------DEBUG------
						}
					}
					if((abilityList.contains("FireProof")) && (getConfig().getBoolean("FireProof") == true)){
						if((event.getCause().equals(DamageCause.FIRE)) || (event.getCause().equals(DamageCause.FIRE_TICK)) || (event.getCause().equals(DamageCause.LAVA))){
							event.setCancelled(true);
						}
					}
					if((abilityList.contains("Water")) && (getConfig().getBoolean("Water") == true)){
						if(event.getCause().equals(DamageCause.DROWNING)){
							//System.out.println(abilityList.toString() + " Contains Water"); //------DEBUG------
							event.setCancelled(true);
							player.setRemainingAir(0);
						}
					}
					Damageable dm = player;
					if((abilityList.contains("Potion")) && (getConfig().getBoolean("Potion") == true)){
						//Drown Cause
						if((abilityList.contains("Water") == false) || (getConfig().getBoolean("Water") == false))
							if(event.getCause().equals(DamageCause.DROWNING))
								if(dm.getHealth() <= dm.getMaxHealth() / 0.3)
									for(ItemStack s : player.getInventory().getContents())
										if(s != null)
											if(s.getTypeId() == 373)
												if((s.getDurability() == 8205) || (s.getDurability() == 8269) || (s.getDurability() == 16397) || (s.getDurability() == 16461)){
													//Apply Potion
													Potion pot = Potion.fromItemStack(s);
													pot.apply(player);
													player.getInventory().remove(s);
													player.sendMessage("§9Your water breathing potion has been auto applied to you.");
													break;
												}
						//Fire Cause
						if((abilityList.contains("FireProof") == false) || (getConfig().getBoolean("FireProof") == false))
							if((event.getCause().equals(DamageCause.FIRE)) || (event.getCause().equals(DamageCause.FIRE_TICK)))
								if(dm.getHealth() <= dm.getMaxHealth() / 0.3)
									for(ItemStack s : player.getInventory().getContents())
										if(s != null)
											if(s.getTypeId() == 373)
												if((s.getDurability() == 8227) || (s.getDurability() == 8195) || (s.getDurability() == 8259) || (s.getDurability() == 16387) || (s.getDurability() == 16451)){
													//Apply Potion
													Potion pot = Potion.fromItemStack(s);
													pot.apply(player);
													player.getInventory().remove(s);
													player.sendMessage("§6Your fire resistance potion has been auto applied to you.");
													break;
												}
						//Poison or Wither
						if((event.getCause().equals(DamageCause.POISON)) || (event.getCause().equals(DamageCause.WITHER)))
							if(dm.getHealth() <= dm.getMaxHealth() / 0.3)
								for(ItemStack s : player.getInventory().getContents())
									if(s != null)
										if(s.getType().equals(Material.MILK_BUCKET)){
											//Drink Milk
											player.getInventory().remove(s);
											player.getInventory().addItem(new ItemStack(Material.BUCKET));
											player.sendMessage("§fYou have auto drank your milk.");
											for(PotionEffect pe : player.getActivePotionEffects())
												player.removePotionEffect(pe.getType());
											break;
										}
						//Other Cause
						ArrayList<Short> healList = new ArrayList<Short>(Arrays.asList((short)8193,(short)8197,(short)8225,(short)8229,(short)8289,(short)16385,(short)16385,(short)16417,(short)16421,(short)16449,(short)16481));
						//System.out.println(dm.getHealth() + " < " + (dm.getMaxHealth() / 2));
						if(dm.getHealth() < (dm.getMaxHealth() / 2))
							for(ItemStack s : player.getInventory().getContents())
								if(s != null)
									if(s.getTypeId() == 373){
										//System.out.println("ID: " + s.getTypeId());
										//System.out.println("DUR: " + s.getDurability());
										//System.out.println("List: " + healList.toString());
										if(healList.contains(s.getDurability())){
											//Apply Potion
											Potion pot = Potion.fromItemStack(s);
											pot.apply(player);
											player.getInventory().remove(s);
											player.sendMessage("§dYour health potion has been auto applied to you.");
											break;
										}
									}
					}
					if((slimeList.contains(player.getName()) == false) && (abilityList.contains("Slime")) && (getConfig().getBoolean("Slime") == true)){
						if(dm.getHealth() - event.getDamage() <= 0){
							event.setCancelled(true);
							dm.setHealth(dm.getMaxHealth()/2);
							slimeList.add(player.getName());
							player.sendMessage("§2You have used your slime DNA.");
							final String name = player.getName();
		    				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			   				     public void run() {
			   				    	 try{
			   				    		slimeList.remove(name);
			   				    		player.sendMessage("§2Your slime DNA has regenerated.");
			   				    	 }catch(Exception x){x.printStackTrace();}
			   					 }
		    				},(getConfig().getInt("slimeCool") * 20));
						}
					}
				}
			}
		}
		
		@EventHandler(priority=EventPriority.HIGH)
		public void inBlockDispense(BlockDispenseEvent e){
			Block b = e.getBlock();
			if(isExtractor(b))
				e.setCancelled(true);
		}
		
		@SuppressWarnings({ "unchecked", "deprecation" })
		@EventHandler(priority=EventPriority.HIGH)
		public void onPlayerDamagedByEntity(EntityDamageByEntityEvent e){
			if(e.getEntity() instanceof Player){
				Player p = (Player) e.getEntity();
				if ((getConfig().getList("enabledworlds").contains(p.getWorld().getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
					ArrayList<String> abilityList = new ArrayList<String>();
			    	if(getSaves().getList("playerList." + p.getName()) != null){
			    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + p.getName());
						if((e.getDamager() instanceof LivingEntity) && (abilityList.contains("Canine")) && (getConfig().getBoolean("Canine") == true)){
							LivingEntity attacker = (LivingEntity)e.getDamager();
							if((attacker instanceof Player) && (getSaves().getList("playerList." + ((Player)attacker).getName()) != null)){
								//Both Are Players
								if(getSaves().getList("playerList." + ((Player)attacker).getName()).contains("Canine"))
									return;
							}
							//Make Wolves Attack
							for(Entity ent : p.getNearbyEntities(15, 15, 15))
								if(ent instanceof Wolf){
									Wolf w = (Wolf) ent;
									w.setTarget(attacker);
								}
						}
						if((e.getDamager() instanceof LivingEntity) && (abilityList.contains("PigMan")) && (getConfig().getBoolean("PigMan") == true)){
							LivingEntity attacker = (LivingEntity)e.getDamager();
							if((attacker instanceof Player) && (getSaves().getList("playerList." + ((Player)attacker).getName()) != null)){
								//Both Are Players
								if(getSaves().getList("playerList." + ((Player)attacker).getName()).contains("PigMan"))
									return;
							}
							//Make Pig Men Attack
							for(Entity ent : p.getNearbyEntities(15, 15, 15))
								if(ent instanceof PigZombie){
									PigZombie pz = (PigZombie) ent;
									pz.setTarget(attacker);
								}
						}
						Damageable dm = p;
						ArrayList<Short> strenghtList = new ArrayList<Short>(Arrays.asList((short)8201,(short)8233,(short)8265,(short)8297,(short)16393,(short)16425,(short)16457,(short)16489));
						if((abilityList.contains("Potion")) && (getConfig().getBoolean("Potion") == true)){
							for(PotionEffect pot : p.getActivePotionEffects())
								if(pot.getType().equals(PotionEffectType.INCREASE_DAMAGE))
									return;
							if(dm.getHealth() <= dm.getMaxHealth() / 0.3)
								for(ItemStack s : p.getInventory().getContents())
									if(s != null)
										if(s.getTypeId() == 373){
											if(strenghtList.contains(s.getDurability())){
												//Apply Potion
												Potion pot = Potion.fromItemStack(s);
												pot.apply(p);
												p.getInventory().remove(s);
												p.sendMessage("§5Your strength potion has been auto applied to you.");
												break;
											}
										}
						}
			    	}
				}
			}
			if((e.getDamager() instanceof Player) && (getSaves().getList("playerList." + ((Player)e.getDamager()).getName()) != null) && (getSaves().getList("playerList." + ((Player)e.getDamager()).getName()).contains("Ebola")) && (getConfig().getBoolean("Ebola") == true)){
				Player p = (Player)e.getDamager();
				//Must Use Fists
				if((p.getItemInHand() == null) || (p.getItemInHand().getType().equals(Material.AIR)))
					if(e.getEntity() instanceof Player){
						//Both Are Players
						Player vic = (Player) e.getEntity();
						if((getSaves().getList("playerList." + vic.getName()) == null) || (getSaves().getList("playerList." + vic.getName()).contains("Ebola") == false)){
							p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (2*25), 1));
							//Infect?
							for(ItemStack s : vic.getInventory().getArmorContents())
								if(s == null)
									addVirus(vic, "Ebola");
						}else
							return;
					}else{
						//Only Attacker Is Player
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, (1*25), 1));
					}
			}
		}
		
		@SuppressWarnings({ "deprecation", "unchecked" })
		@EventHandler(priority=EventPriority.HIGH)
		public void onPlayerClickEntity(PlayerInteractEntityEvent e){
			Player p = e.getPlayer();
			if ((getConfig().getList("enabledworlds").contains(p.getWorld().getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
		    	if((getSaves().getList("playerList." + p.getName()) != null) && (e.getRightClicked() instanceof Villager)){
		    		if((getSaves().getList("playerList." + p.getName()).contains("Villager")) && (getConfig().getBoolean("Villager") == true)){
		    			//Check Time
		    			if(villagerWait.contains(p.getName()) == false){
		    				villagerWait.add(p.getName());
		    				//Get Random Item
		    				ArrayList<ItemStack> itemList = new ArrayList<ItemStack>();
		    				for(String s : (ArrayList<String>)getConfig().getList("VillagerItems")){
		    					String[] split = s.split(":");
		    					int id = Integer.parseInt(split[0]);
		    					int amount = Integer.parseInt(split[1]);
		    					itemList.add(new ItemStack(id, amount));
		    				}
		    				int index = new Random().nextInt(itemList.size());
		    				ItemStack stack = itemList.get(index);
		    				p.getInventory().addItem(stack);
		    				p.sendMessage(getConfig().getString("VillagerMessage").replace("&", "§"));
		    				p.updateInventory();
		    				p.closeInventory();
		    				e.setCancelled(true);
		    				final String name = p.getName();
		    				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			   				     public void run() {
			   				    	 try{
			   				    		villagerWait.remove(name);
			   				    	 }catch(Exception x){x.printStackTrace();}
			   					 }
		    				},(getConfig().getInt("villagerGiftTime") * 20));
		    			}
		    		}
				}
			}
		}
		
		@EventHandler(priority=EventPriority.HIGH)
	    public void onPlayerJoin(PlayerJoinEvent event){
			final Player player = event.getPlayer();
			join(player);
		}
		
		@SuppressWarnings("unchecked")
		public void join(final Player player){
			if(stoneList.contains(player.getName()))
				stoneList.remove(player.getName());
			World world = player.getWorld();
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
				addUser(player);
			}
			ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + player.getName()) != null){
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
	    	}
			if ((abilityList.contains("Speed")) && (getConfig().getBoolean("Speed") == true)){
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				     public void run() {
				    	World world = player.getWorld();
						if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {	
							//System.out.println("LastY = " + userGet(player).getLastY); //------DEBUG------
							if(userGet(player).getLastY != 1){
								//System.out.println("Speed1"); //------DEBUG------
					    		 player.setWalkSpeed(player.getWalkSpeed()*getConfig().getInt("speedSpeed"));
					    		 userGet(player).setUserY(1);
					    	 }
						}
					 }
				}, (1 * 20));
			}
		}
		
		public void setHearts(Player player){
			Damageable dm = player;
			if(dm.getMaxHealth() < 60){
				if(dm.getMaxHealth() + 10 <= 40)
					player.setMaxHealth(dm.getMaxHealth() + 10);
				else
					player.setMaxHealth(dm.getMaxHealth());
			}
		}
		
		public void takeHearts(Player player){
			Damageable dm = player;
			if(dm.getMaxHealth() > 20){
				if(dm.getMaxHealth() - 10 >= 20)
					player.setMaxHealth(dm.getMaxHealth() - 10);
				else
					player.setMaxHealth(20);
			}
		}
		
		public void stopCooling(final Player player, final String coolingOject, final int time){
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			     public void run() {
			    	 try{
						int index = findUserIndex(player);
						if(index != -1){
							AbilityUser user = userGet(player);
				    		if(coolingOject.equals("FireBall")){
				    			user.setFire(false);
				    		}else if(coolingOject.equals("Milk")){
				    			user.setMilk(false);
				    		}else if(coolingOject.equals("Teleport")){
				    			user.setTeleport(false);
				    		}else if(coolingOject.equals("Woolly")){
				    			user.setWool(false);
				    		}else if(coolingOject.equals("Flying")){
				    			user.setFly(false);
				    		}else{
				    			System.out.println(coolingOject + " is not a valid ability!");
				    			return;
				    		}
					    	player.sendMessage("§f" + coolingOject + ": §7Recharged.");
						}else
							addUser(player);
			    	 }catch(Exception x){};
				 }
			}, (300 * 20));
		}
		
		public void cool(Player player, String coolingOject, int time){
			int index = findUserIndex(player);
			if(index != -1){
				AbilityUser user = userGet(player);
	    		if(coolingOject.equals("FireBall")){
	    			user.setFire(true);
	    		}else if(coolingOject.equals("Milk")){
	    			user.setMilk(true);
	    		}else if(coolingOject.equals("Teleport")){
	    			user.setTeleport(true);
	    		}else if(coolingOject.equals("Woolly")){
	    			user.setWool(true);
	    		}else if(coolingOject.equals("Flying")){
	    			user.setFly(true);
	    		}else{
	    			System.out.println(coolingOject + " is not a valid ability!");
	    			return;
	    		}
	    		stopCooling(player, coolingOject, time);
			}else
				addUser(player);
		}
		
		public void addUser(Player player){
			if(findUserIndex(player) == -1){
				AbilityUser user = new AbilityUser(player);
				abilityUserList.add(user);
			}
		}
		
		@SuppressWarnings("deprecation")
		@EventHandler(priority=EventPriority.HIGH)
	    public void onPlayerSneak(PlayerToggleSneakEvent e){
			Player p = e.getPlayer();
			if ((getConfig().getList("enabledworlds").contains(p.getWorld().getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
		    	if(getSaves().getList("playerList." + p.getName()) != null)
					if((getSaves().getList("playerList." + p.getName()).contains("Stone")) && (getConfig().getBoolean("Stone") == true)){
						//Stone Sneak
						if((p.isSneaking() == false) && (stoneList.contains(p.getName()) == false) && ((p.getLocation().getBlock().getType().equals(Material.AIR)) || (transBlocks.contains(p.getLocation().getBlock().getType().getId())))){
							Location l0 = p.getLocation().clone();
							l0.setY(l0.getBlockY() - 1);
							Location l = p.getLocation().clone();
							//l.setX(l.getBlockX());
							l.setY(l.getBlockY() + 1);
							//l.setZ(l.getBlockZ());
							Location l2 = l.clone();
							l2.setY(l2.getBlockY() + 1);
							if((l0.getBlock().getType().equals(Material.AIR)) &&  ((l.getBlock().getType().equals(Material.AIR)) || (transBlocks.contains(l.getBlock().getType().getId()))) && ((l2.getBlock().getType().equals(Material.AIR)) || (transBlocks.contains(l2.getBlock().getType().getId())))){
								//On
								p.getLocation().getBlock().setType(Material.STONE);
								stoneList.add(p.getName());
								p.teleport(l);
								freeze(p);
							}
						}else if(stoneList.contains(p.getName())){
							//Off
							stopHide(p);
						}
					}
			}
		}
		
		public void stopHide(Player p){
			Location l = p.getLocation().clone();
			l.setY(l.getBlockY() - 1);
			l.getBlock().setType(Material.AIR);
			stoneList.remove(p.getName());
			p.teleport(l);
			unfreeze(p);
		}
		
	    public void freeze(Player player) {
	        //player.setWalkSpeed(0.0F);
	        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 99999, 128));
	        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 99999, 128));
	        player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 99999, 1));
	    }
	 
	    public void unfreeze(Player player) {
	        //player.setWalkSpeed(0.2F);
	        player.removePotionEffect(PotionEffectType.JUMP);
	        player.removePotionEffect(PotionEffectType.SLOW);
	        player.removePotionEffect(PotionEffectType.INVISIBILITY);
	    }
		
		@SuppressWarnings({ "unchecked" })
		@EventHandler(priority=EventPriority.HIGH)
	    public void onPlayerDie(PlayerDeathEvent event){
			Player player = event.getEntity();
			World world = player.getWorld();
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
				ArrayList<String> abilityList = new ArrayList<String>();
		    	if(getSaves().getList("playerList." + player.getName()) != null)
		    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
				if((abilityList.contains("Explode")) && (getConfig().getBoolean("Explode") == true)){
					player.getWorld().createExplosion(player.getLocation(), 5);	
				}
				if (getConfig().getBoolean("resetAbilitiesOnDeath") == true){
					//Remove All Abilities
					removeAllAbilities(player);
				}else{
					//Just Remove Sickness
					for(String s : virusList)
						if(abilityList.contains(s))
							abilityList.remove(s);
					getSaves().set("playerList." + player.getName(), abilityList);
					saveSaves();
				}
			}
		}
		
		@SuppressWarnings({ "unchecked" })
		private void removeAllAbilities(Player player){
			ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + player.getName()) != null)
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
			if((abilityList.contains("Health")) && (getConfig().getBoolean("Health") == true))
				player.setMaxHealth(20);
			if ((abilityList.contains("Speed")) && (getConfig().getBoolean("Speed") == true)){
				if(userGet(player).getLastY == 1){
					player.setWalkSpeed(player.getWalkSpeed()/2);
					userGet(player).setUserY(0);
				}
			}
			getSaves().set("playerList." + player.getName(), null);
			saveSaves();
		}
		
		@SuppressWarnings("unchecked")
		public boolean hasFlightPerms(Player player){
			if(getConfig().getList("flyingPerms") != null){
				ArrayList<String> permList = (ArrayList<String>) getConfig().getList("flyingPerms");
				for(String perm : permList){
					if(player.hasPermission(perm)){
						return true;
					}
				}
			}
			return false;
		}
		
		@SuppressWarnings("unchecked")
		public void startParticles(World world){
			//System.out.println("starting p in " + world.getName()); //------DEBUG------
			ArrayList<String> stringList = new ArrayList<String>();
			//System.out.println("sp2"); //------DEBUG------
			stringList = (ArrayList<String>) getSaves().getList("saveNameList");
			if(stringList != null){
				for(String sNum : stringList){
					if(getSaves().getString("centrifugeList." + world.getName() + "." + sNum) != null){		
						double x = getSaves().getDouble("centrifugeList." + world.getName() + "." + sNum + ".x");
						double y = getSaves().getDouble("centrifugeList." + world.getName() + "." + sNum + ".y");
						double z = getSaves().getDouble("centrifugeList." + world.getName() + "." + sNum + ".z");
						Location location = new Location(world, x, y, z);
						if((displayingParticles(location) == false) && (isCentrifuge(location.getBlock()) == true))
							showCentrifugeParticles(location);
					}else if(getSaves().getString("breederList." + world.getName() + "." + sNum) != null){
						double x = getSaves().getDouble("breederList." + world.getName() + "." + sNum + ".x");
						double y = getSaves().getDouble("breederList." + world.getName() + "." + sNum + ".y");
						double z = getSaves().getDouble("breederList." + world.getName() + "." + sNum + ".z");
						Location location = new Location(world, x, y, z);
						if((displayingParticles(location) == false) && (isBreeder(location.getBlock()) == true))
							showBreederParticles(location);
					}
				}
			}else{
				System.out.println("No saved centrifuges found!");
			}
		}

		@SuppressWarnings("unchecked")
		@EventHandler(priority=EventPriority.HIGH)
		public void onPlayerTeleport(PlayerTeleportEvent event) {
			final Player player = event.getPlayer();
			World world = player.getWorld();
			startParticles(world);
			ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + player.getName()) != null){
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
	    	}
			if ((abilityList.contains("Speed")) && (getConfig().getBoolean("Speed") == true)){
				if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {	
					//System.out.println("LastY = " + userGet(player).getLastY); //------DEBUG------
					if(userGet(player).getLastY != 1){
						//System.out.println("Speed2"); //------DEBUG------
						Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
						     public void run() {
							    player.setWalkSpeed(player.getWalkSpeed()*2);
							 }
						}, (1 * 20));
						userGet(player).setUserY(1);
					}
				}
			}
		}
		
		@EventHandler(priority=EventPriority.HIGH)
		public void onPlayerChangedWorld(PlayerChangedWorldEvent event) {
			World world = event.getPlayer().getWorld();
			startParticles(world);
		}
	    
		public boolean displayingParticles(Location loc){
			if(getMachineBlock(getCoordName(loc)) != null)
				return true;
			machineBlock mb = new machineBlock(loc, getCoordName(loc), true);
			machineBlockList.add(mb);
			return false;
		}
		
		public void inject(Player injector, Player injected){
			String dna = null;
        	boolean anti = false;
        	boolean vaccine = false;
			for(String loreLine : injector.getItemInHand().getItemMeta().getLore()){
				for(String dnaType : dnaTypesList){
					if(loreLine.contains(dnaType)){
						dna = dnaType;
					}
				}
				if(loreLine.contains("Anti"))
					anti = true;
				if(loreLine.contains("Vaccine"))
					vaccine = true;
			}
			//System.out.println("Anti = " + anti);
			if((dna != null) && (getConfig().getBoolean(dna.replace(" ", "")) == true)){
				int amount = injector.getItemInHand().getAmount() - 1;
				if(amount <= 0){
					injector.setItemInHand(null);
				}else{
					injector.getItemInHand().setAmount(amount);
				}
				//Virus Check
				if(virusList.contains(dna)){
					//Is Virus
					if(vaccine == true){
						removeVirus(injected, dna);
					}else
						addVirus(injected, dna);
				}else
					if(anti == true){
						removeDna(injected, injector, dna);
					}else
						addDna(injected, injector, dna);
				if(injected.equals(injector))
					((LivingEntity) injected).addPotionEffect(new PotionEffect(PotionEffectType.HARM, 1, 0), true);
				((LivingEntity) injected).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 250, 1), true);
			}
		}
		
		@SuppressWarnings("unchecked")
		public void addVirus(Player p, String dna){
			//Message
			p.sendMessage("§7You have been infected with " + dna + " §7virus!");
			//Save
	    	ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + p.getName()) != null)
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + p.getName());
	    	if(virusList.contains(dna)){
		    	abilityList.add(dna);
		    	getSaves().set("playerList." + p.getName(), abilityList);
	    		saveSaves();
	    	}
		}
		
		@SuppressWarnings("unchecked")
		public void removeVirus(Player p, String dna){
			//Message
			p.sendMessage("§7You have been cured from " + dna + " §7virus!");
			//Save
	    	ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + p.getName()) != null)
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + p.getName());
	    	if(virusList.contains(dna)){
		    	abilityList.remove(dna);
		    	getSaves().set("playerList." + p.getName(), abilityList);
	    		saveSaves();
	    	}
	    	for(PotionEffect ef : p.getActivePotionEffects())
	    		p.removePotionEffect(ef.getType());
		}
		
		@SuppressWarnings({ "deprecation", "unchecked" })
		@EventHandler(priority=EventPriority.HIGH)
	    public void onPlayerInteract(PlayerInteractEvent event) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
			final Player player = event.getPlayer();
			World world = player.getWorld();
			//System.out.println("x1"); //------DEBUG------
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
	            if((player.getItemInHand().getType().equals(Material.POTION)) && (player.getItemInHand().getItemMeta().getDisplayName() != null)){
	            	if(player.getItemInHand().getItemMeta().getDisplayName().equals("§3Vial of blood")){
	            		checkDrink(player, player.getInventory().getHeldItemSlot());
	            	}
	            }else if((player.getItemInHand().getType().equals(Material.SNOW_BALL)) && (player.getItemInHand().getItemMeta() != null) && (player.getItemInHand().getItemMeta().getDisplayName() != null)){
	            	for(String mob : mobList){
	            		if (player.getItemInHand().getItemMeta().getDisplayName().equals("§3" + mob + " Cell")){
	            			player.sendMessage("§cYou can't throw this!");
	            			event.setCancelled(true);
	            			player.updateInventory();
	            		}
	            	}
	            }else if((player.getItemInHand().getType().equals(Material.ENDER_PEARL)) && (player.getItemInHand().getItemMeta() != null) && (player.getItemInHand().getItemMeta().getDisplayName() != null)){
            		if (player.getItemInHand().getItemMeta().getDisplayName().equals("§dAnti-Cell")){
            			player.sendMessage("§cYou can't throw this!");
            			event.setCancelled(true);
            			player.updateInventory();
            		}
	            }
	            if((player.getItemInHand().getItemMeta() != null) && (player.getItemInHand().getItemMeta().getDisplayName() != null) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§3Filled Syringe"))){
	            	if((event.getAction().equals(Action.RIGHT_CLICK_AIR)) || (event.getAction().equals(Action.RIGHT_CLICK_BLOCK))){
		            	//Syringe Use
		            	inject(player,player);
						event.setCancelled(true);
	            	}
				}
	            if((event.getClickedBlock() != null) && (event.getClickedBlock().getType() == Material.STONE_BUTTON)){
					//System.out.println("Stone1"); //------DEBUG------
		            Block buttonBlock = event.getClickedBlock();
		            Button button = (Button)buttonBlock.getState().getData();
		            Block block = buttonBlock.getRelative(button.getAttachedFace());
		            //System.out.println("2"); //------DEBUG------         
					if((block != null) && (block.getType() == Material.FURNACE)){
						Furnace furnace = (Furnace) block.getState();
						Location under = furnace.getLocation().clone();
						under.setY(under.getY() - 1);
						if ((furnace.getInventory().getType() != null) && (furnace.getInventory().getType().equals(InventoryType.FURNACE))){
							if (isAnalyzer(block) && (furnace.getInventory().getItem(0) != null)){
								//System.out.println("Under = " + under.getBlock().getType()); //------DEBUG------      
								if (((furnace.getInventory().getItem(1) != null) && (furnace.getInventory().getItem(1).getType().equals(Material.WATER_BUCKET))) || (under.getBlock().getType().equals(Material.STATIONARY_WATER))){
									if ((furnace.getInventory().getItem(0).getType().equals(Material.PUMPKIN_SEEDS)) || (furnace.getInventory().getItem(0).getType().equals(Material.POTION))){
										//(furnace.getInventory().getItem(2) == null)
										ItemStack cell = null;
										ItemStack item = furnace.getInventory().getItem(0);
										if((item.getItemMeta().getDisplayName() != null) && (item.getType().equals(Material.PUMPKIN_SEEDS))){
											///Take 1 Skin
											int amount = furnace.getInventory().getItem(0).getAmount() - 1;
											if (amount <= 0){
												furnace.getInventory().setItem(0, null);
											}else{
												furnace.getInventory().getItem(0).setAmount(amount);
											}
											for(String mob : mobList){
												if(item.getItemMeta().getDisplayName().equals("§3" + mob + " Skin")){
													cell = setItem(Material.SNOW_BALL, "§3" + mob + " Cell", "§8Cell from a " + mob);
												}
											}
											if((cell != null) && ((furnace.getInventory().getItem(2) == null) || ((furnace.getInventory().getItem(2).getItemMeta() != null) && (furnace.getInventory().getItem(2).getItemMeta().getDisplayName().equals(cell.getItemMeta().getDisplayName()))))){
												//Add Cell
												if(furnace.getInventory().getItem(2) != null){
													cell.setAmount(furnace.getInventory().getItem(2).getAmount() + 1);
													furnace.getInventory().setItem(2, cell);
												}else
													furnace.getInventory().setItem(2, cell);
												Location tank = block.getLocation();
												tank.setY(tank.getY() + 1);
												block.getWorld().playEffect(tank, Effect.STEP_SOUND, 79);
											}
										}else if((item.getItemMeta().getDisplayName() != null) && (item.getType().equals(Material.POTION)) && (item.getItemMeta().getDisplayName().equals("§3Vial of blood"))){
											String name = null;
											for(String loreLine : item.getItemMeta().getLore()){
												if(loreLine.contains("§8Blood from ")){
													name = loreLine;
												    name = name.replace("§8Blood from ", "");
												   if(name.startsWith("§")){
													   name = name.substring(2);
												   }
												   name = name.substring(0, name.length()-2);
												}
											}
											if(name != null){
								    			if(getPlayerDna(player) != null){
								    				String playerDna = getPlayerDna(player);
								    				player.sendMessage("§7Analyzer: §7" + name + "§7 has the genetic changes: " + playerDna);
								    			}else{
								    				player.sendMessage("§7Analyzer: §7" + name + " §7Has no genetic changes.");
								    			}
											}else{
												player.sendMessage("§7Analyzer: §cError! Not player blood!");
											}
											Location tank = block.getLocation();
											tank.setY(tank.getY() + 1);
											block.getWorld().playEffect(tank, Effect.STEP_SOUND, 79);
											furnace.getInventory().setItem(0, new ItemStack(Material.GLASS_BOTTLE, 1));
										}
										//Water Removal
										//Bucket \/
										if((furnace.getInventory().getItem(1) != null) && (furnace.getInventory().getItem(1).getType().equals(Material.WATER_BUCKET))){
											furnace.getInventory().getItem(1).setType(Material.BUCKET);
										}else
											under.getBlock().setType(Material.AIR);
										//Source /\
									}
								}
							}
						}
					}
				}else if((event.getClickedBlock() != null) && (event.getClickedBlock().getType() == Material.LEVER)){
					//System.out.println("l1"); //------DEBUG------
		            Block leverBlock = event.getClickedBlock();
		            Lever lever = (Lever)leverBlock.getState().getData();
		            Block block = leverBlock.getRelative(lever.getAttachedFace());
					//System.out.println("l2 " + block.getType()); //------DEBUG------
					if((block != null) && (block.getType() == Material.DISPENSER)){
						Dispenser dispenser = (Dispenser) block.getState();
						int input =  3;
						int fuel = 7;
						int output = 5;
						//System.out.println("l3"); //------DEBUG------
						if ((dispenser.getInventory() != null) && (dispenser.getInventory().getType().equals(InventoryType.DISPENSER))){
							//System.out.println("l3.2"); //------DEBUG------
							if ((dispenser.getInventory().getItem(input) != null) && (dispenser.getInventory().getItem(fuel) != null) && (dispenser.getInventory().getItem(output) == null)){
								//System.out.println("l3.3"); //------DEBUG------
								if ((dispenser.getInventory().getItem(fuel).getType().equals(Material.EYE_OF_ENDER)) && isExtractor(block)){
									//System.out.println("l3.4"); //------DEBUG------
									ItemStack item = dispenser.getInventory().getItem(input);
									ItemStack dna = null;
									//System.out.println("l4"); //------DEBUG------
									if((item.getItemMeta().getDisplayName() != null) && (!item.getType().equals(Material.AIR)) && (item.getType().equals(Material.SNOW_BALL))){
										int amount = dispenser.getInventory().getItem(input).getAmount() - 1;
										if (amount <= 0){
											dispenser.getInventory().setItem(input, null);
										}else{
											dispenser.getInventory().getItem(input).setAmount(amount);
										}
										int fuelAmount = dispenser.getInventory().getItem(fuel).getAmount() - 1;
										if (fuelAmount <= 0){
											dispenser.getInventory().setItem(fuel, null);
										}else{
											dispenser.getInventory().getItem(fuel).setAmount(fuelAmount);
										}
										//System.out.println("l5"); //------DEBUG------
										Random rand = new Random();
										int min = 1;
										int max = 10;
										int randomNum = rand.nextInt(max - min + 1) + min;
										if(item.getItemMeta().getDisplayName().contains("§3Bat")){
											if(randomNum == 9){
												dna = setItem(Material.STRING, "§3Flying DNA", "§8DNA for the Flying ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Blaze")){
											if(randomNum <= 2){
												dna = setItem(Material.STRING, "§3FireBall DNA", "§8DNA for the FireBall ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3Flying DNA", "§8DNA for the Flying ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3CaveSpider")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Poison DNA", "§8DNA for the Poison ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3Climb DNA", "§8DNA for the Climb ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Chicken")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3NoFall DNA", "§8DNA for the NoFall ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Cow")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Milk DNA", "§8DNA for the Milk ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3EatGrass DNA", "§8DNA for the EatGrass ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Creeper")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Explode DNA", "§8DNA for the Explode ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3EnderMan")){
											if(randomNum == 6){
												dna = setItem(Material.STRING, "§3Teleport DNA", "§8DNA for the Teleport ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Ghast")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3FireBall DNA", "§8DNA for the FireBall ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3Flying DNA", "§8DNA for the Flying ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Horse")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Jump DNA", "§8DNA for the Jump ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3EatGrass DNA", "§8DNA for the EatGrass ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Spider")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Climb DNA", "§8DNA for the Climb ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3VillagerGolem")){
											if(randomNum == 3){
												dna = setItem(Material.STRING, "§3Health DNA", "§8DNA for the Health ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3NoFall DNA", "§8DNA for the NoFall ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Ozelot")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Speed DNA", "§8DNA for the Speed ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3PigZombie")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3PigMan DNA", "§8DNA for the PigMan ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Pig")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3EatGrass DNA", "§8DNA for the EatGrass ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3LavaSlime")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3FireProof DNA", "§8DNA for the FireProof ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Sheep")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Woolly DNA", "§8DNA for the Woolly ability", "§8Bred 0 times");
											}else if(randomNum == 10){
												dna = setItem(Material.STRING, "§3EatGrass DNA", "§8DNA for the EatGrass ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3WitherSkeleton")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Wither DNA", "§8DNA for the Wither ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Skeleton")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3DayBurn DNA", "§8DNA for the DayBurn ability", "§8Bred 0 times");
											}else if (randomNum == 10){
												dna = setItem(Material.STRING, "§3Undead DNA", "§8DNA for the Undead ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Zombie")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3DayBurn DNA", "§8DNA for the DayBurn ability", "§8Bred 0 times");
											}else if (randomNum == 10){
												dna = setItem(Material.STRING, "§3Undead DNA", "§8DNA for the Undead ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Squid")){
											if(randomNum == 6){
												dna = setItem(Material.STRING, "§3Water DNA", "§8DNA for the Water Breathing ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Villager")){
											if(randomNum <= 3){
												dna = setItem(Material.STRING, "§3Villager DNA", "§8DNA for the Villager ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Wolf")){
											if(randomNum <= 2){
												dna = setItem(Material.STRING, "§3Canine DNA", "§8DNA for the Canine ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3SilverFish")){
											if(randomNum == 1){
												dna = setItem(Material.STRING, "§3Stone DNA", "§8DNA for the Stone ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Slime")){
											if(randomNum == 3){
												dna = setItem(Material.STRING, "§3Slime DNA", "§8DNA for the Slime ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}else if(item.getItemMeta().getDisplayName().contains("§3Witch")){
											if(randomNum == 3){
												dna = setItem(Material.STRING, "§3Potion DNA", "§8DNA for the Potion ability", "§8Bred 0 times");
											}else{
												dna = setItem(Material.STRING, "§3Common DNA", "§8Common DNA with no ability", "§8Bred 0 times");
											}
										}
										//System.out.println("Ocelot: " + dna);
										if (dna != null){
											dispenser.getInventory().setItem(output, dna);
											Location tank = block.getLocation();
											tank.setY(tank.getY() + 1);
											block.getWorld().playEffect(tank, Effect.STEP_SOUND, 152);
										}else{
											System.out.println("ERROR 828821! " + item.getItemMeta().getDisplayName()); //------DEBUG------
										}
									}
								}
							}
						}
					}
				}else if((event.getClickedBlock() != null) && (event.getClickedBlock().getType() == Material.DISPENSER)){
					Dispenser d = (Dispenser) event.getClickedBlock().getState();
					if(isExtractor(event.getClickedBlock()) == false)
						for(ItemStack i : d.getInventory())
							if(i != null)
								try{
									if(i.getItemMeta().getDisplayName().contains("§8<-In|Out->")){
										//Block is wrecked extractor
										removeExtractor(d, d.getBlock(),3,7,5);
									}
								}catch(Exception x){x.printStackTrace();}
				}else if((event.getClickedBlock() != null) && (event.getClickedBlock().getType() == Material.STAINED_GLASS) && event.getAction().equals(Action.RIGHT_CLICK_BLOCK)){
					//System.out.println("Stain Click"); //------DEBUG------
					if((isSave(world, event.getClickedBlock().getLocation(), "centrifugeList") == true) && (isCentrifuge(event.getClickedBlock()) == true)){
						//Centrifuge
						Inventory inventory = this.getServer().createInventory(player, InventoryType.BREWING, "Centrifuge");
					    player.openInventory(inventory);
					}else if((isSave(world, event.getClickedBlock().getLocation(), "breederList") == true) && (isBreeder(event.getClickedBlock()) == true)){
						//Breeder
						Inventory inventory = this.getServer().createInventory(player, InventoryType.HOPPER, "Breeder");
					    player.openInventory(inventory);
					}/**Weird Not Working Thingy**//*else{
						//System.out.println("Glass");
						Location baseLoc = event.getClickedBlock().getLocation();
						baseLoc.setY(baseLoc.getY() - 1);
						Block base = baseLoc.getBlock();
						try{
					    	//Analyzer
							if(base.getType().equals(Material.FURNACE)){
								Furnace f = (Furnace) base.getState();
								if (isAnalyzer(base))
									player.openInventory(f.getInventory());
							}
					    	//Extractor
							if (base.getType().equals(Material.DISPENSER)){
								Dispenser d = (Dispenser) base.getState();
								if (isExtractor(base))
									player.openInventory(d.getInventory());
							}
						}catch(Exception x){x.printStackTrace();}
					}*/
				}else if ((event.getAction().equals(Action.RIGHT_CLICK_AIR)) && (player.getItemInHand().getItemMeta() != null) && (player.getItemInHand().getItemMeta().getDisplayName() != null) && (player.getItemInHand().getItemMeta().getDisplayName().equals("§3Syringe"))){
					ItemStack bottles = null;
					for(ItemStack posBot : player.getInventory()){
						if((posBot != null) && (posBot.getType().equals(Material.GLASS_BOTTLE))){
							bottles = posBot;
						}
					}
					if(bottles != null){
						ItemStack drop = getBlood(player.getDisplayName(), true);
						if((bottles.getAmount() - 1) <= 0){
							player.getInventory().removeItem(bottles);
						}else{
							bottles.setAmount(bottles.getAmount() - 1);
						}
						int syringeAmount = player.getItemInHand().getAmount() - 1;
						if(syringeAmount <= 0){
							player.getInventory().removeItem(player.getItemInHand());
						}else{
							player.getItemInHand().setAmount(syringeAmount);
						}
						player.getInventory().addItem(drop);
						player.updateInventory();
						((LivingEntity) player).addPotionEffect(new PotionEffect(PotionEffectType.HARM, 1, 0), true);
						((LivingEntity) player).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 250, 1), true);
					}
				}
			}
			//------------POWERS---------------
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
				try{
					if (player.isSneaking()){
						ArrayList<String> abilityList = new ArrayList<String>();
				    	if(getSaves().getList("playerList." + player.getName()) != null){
				    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + player.getName());
				    		if((player.getItemInHand().getType().equals(Material.AIR)) && (event.getAction().equals(Action.LEFT_CLICK_AIR)) && (abilityList.contains("FireBall")) && (getConfig().getBoolean("FireBall") == true)){
					    		if(userGet(player).getFireBallCooling == false){
						    		((LivingEntity) player).launchProjectile(Fireball.class);
						    		cool(player, "FireBall", 20);
						    	}
					    	}
					    	if((player.getItemInHand().getType().equals(Material.AIR)) && (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)) && (abilityList.contains("Teleport")) && (getConfig().getBoolean("Teleport") == true)){
					    		if((userGet(player).getTeleportCooling == false) && (userGet(player).getFlyCooling == false)){
					    			cool(player, "Flying", 5);
					    			player.sendMessage("§9Teleport: §7Right-Click the ground again to confirm.");
					    		}else if((userGet(player).getTeleportCooling == false) && (userGet(player).getFlyCooling == true)){
					    			player.sendMessage("§9Teleport: §7Confirmed, look where you want to teleport.");
						    		cool(player, "Teleport", 15);
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
									     public void run() {
									    	 player.sendMessage("§9Teleport: §7Commencing in 3...");
										 }
									}, (4 * 20));
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
									     public void run() {
									    	 player.sendMessage("§9Teleport: §72...");
										 }
									}, (5 * 20));
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
									     public void run() {
									    	 player.sendMessage("§9Teleport: §71...");
										 }
									}, (6 * 20));
									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
									     public void run() {
									    	player.sendMessage("§9Teleport: §7Teleporting...");
									    	Location tpPoint = player.getTargetBlock((HashSet<Byte>)null, 200).getLocation();
									    	tpPoint.setY(tpPoint.getY() +1);
									    	player.teleport(tpPoint);
										 }
									}, (7 * 20));
//									Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
//									     public void run() {
//									    	 stopCooling(player, "Teleport");
//									    	 player.sendMessage("§9Teleport: §7Recharged.");
//										 }
//									}, (15 * 20));
					    		}
					    	}
					    	if((player.getItemInHand().getType().equals(Material.BUCKET)) && (event.getAction().equals(Action.RIGHT_CLICK_AIR)) && (abilityList.contains("Milk")) && (getConfig().getBoolean("Milk") == true)){
					    		if(userGet(player).getMilkCooling == false){
						    		int amount = player.getItemInHand().getAmount() - 1;
						    		ItemStack milk = new ItemStack(Material.MILK_BUCKET, 1);
						    		if(amount <= 0){
						    			player.setItemInHand(milk);
						    		}else{
						    			player.getItemInHand().setAmount(amount);
						    			player.getInventory().addItem(milk);
						    		}
						    		player.updateInventory();
						    		cool(player, "Milk", 30);
					    		}
					    	}
					    	if((player.getItemInHand().getType().equals(Material.SHEARS)) && (event.getAction().equals(Action.RIGHT_CLICK_AIR)) && (abilityList.contains("Woolly")) && (getConfig().getBoolean("Woolly") == true)){
					    		if(userGet(player).getWoolCooling == false){
						    		int randomNum = new Random().nextInt(3 - 1 + 1) + 1;
						    		ItemStack wool = new ItemStack(Material.WOOL, randomNum);
						    		player.getWorld().dropItemNaturally(player.getLocation(), wool);
						    		player.getItemInHand().setDurability((short) (player.getItemInHand().getDurability() + 50));
						    		player.updateInventory();
						    		cool(player, "Woolly", 300);
					    		}
					    	}
					    	//System.out.println("Player has " + player.getItemInHand().getType()); //------DEBUG------
					    	//System.out.println("Action " + event.getAction()); //------DEBUG------
					    	//System.out.println("Eat Grass config: " + getConfig().getBoolean("EatGrass")); //------DEBUG------
					    	//System.out.println("Player abilities: " + abilityList.toString()); //------DEBUG------
					    	if((player.getItemInHand().getType().equals(Material.AIR)) && (event.getAction().equals(Action.LEFT_CLICK_BLOCK)) && (abilityList.contains("EatGrass")) && (getConfig().getBoolean("EatGrass") == true)){
					    		Block eaten = player.getTargetBlock((HashSet<Byte>)null, 5);
					    		//System.out.println("Eaten: " + eaten.getType()); //------DEBUG------
					    		if(eaten.getType().equals(Material.GRASS)){
					    			eaten.setType(Material.DIRT);
					    			eaten.getWorld().playEffect(eaten.getLocation(), Effect.STEP_SOUND, 2);
					    			player.setFoodLevel(player.getFoodLevel()+1);
					    		}
					    	}
				    	}
			    	}
				}catch(Exception x){addUser(player);}
			}
		}
		
		public ItemStack getBlood(String name, boolean isName){
			ItemStack drop = (new ItemStack(Material.POTION, 1, (short) 8201));
			ItemMeta poisionMeta = drop.getItemMeta();
			ArrayList<String> poisionLoreList = new ArrayList<String>();
			poisionMeta.setDisplayName("§3Vial of blood");
			if(isName == false){
				poisionLoreList.add("§8" + name + " §8blood");
			}else{
				poisionLoreList.add("§8Blood from " + name);
			}
			poisionMeta.setLore(poisionLoreList);
			drop.setItemMeta(poisionMeta);
			EnchantGlow.addGlow(drop);
			return drop;
		}
		
		public void checkDrink(final Player p, final int slot){
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
			     public void run() {
				        if((p.getInventory().getHeldItemSlot() == slot) && (p.getItemInHand().getType().equals(Material.GLASS_BOTTLE))){
				        	if (p.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE) == true){
				        		p.removePotionEffect(PotionEffectType.INCREASE_DAMAGE);
				        		((LivingEntity) p).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 300, 1), true);
				        		((LivingEntity) p).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 100, 1), true);
				        		//p.sendMessage("Checked Drink");
				        	}
				        }
				 }
			}, (35));
		}
	    
		@SuppressWarnings("deprecation")
		@EventHandler(priority=EventPriority.HIGH)
		public void onPlayerClick(InventoryClickEvent event) {
			int rawSlot = event.getRawSlot();
			Player player = (Player) event.getWhoClicked();
			World world = player.getWorld();
			//Check World Enabled
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
				//Holding Valid Item
				if((player.getItemOnCursor() != null) && (!player.getItemOnCursor().getType().equals(Material.AIR)) && (event.getCurrentItem() != null) && (!event.getCurrentItem().getType().equals(Material.AIR))){
					//Has Display Name
					if((player.getItemOnCursor().getItemMeta().getDisplayName() != null) && (event.getCurrentItem().getItemMeta().getDisplayName() != null)){
						String sourceName = player.getItemOnCursor().getItemMeta().getDisplayName();
						if((sourceName.contains("§3Vial of")) || (sourceName.equals("§3Filled Syringe"))){
							String destName = event.getCurrentItem().getItemMeta().getDisplayName();
							ItemStack source = player.getItemOnCursor();
							ItemStack dest = event.getCurrentItem();
							if((destName.equals("§3Syringe")) && (sourceName.contains("§3Vial of")) || (destName.equals("§3Syringe"))){
								String dna = null;
								boolean anti = false;
								boolean vaccine = false;
								if(sourceName.contains("anti"))
									anti= true;
								for(String loreLine : source.getItemMeta().getLore()){
									for(String dnaType : dnaTypesList){
										//System.out.println(loreLine + " does not contain " + dnaType);
										if(loreLine.contains(dnaType)){
											dna = dnaType;
										}
									}
									if(loreLine.contains("vaccine"))
										vaccine = true;
								}
								if(dna != null){
									ItemStack filledSyringe;
									if(anti == true){
										filledSyringe = setItem(Material.TRIPWIRE_HOOK, "§3Filled Syringe", "§8Left/Right-Click to inject", "§8Contains: Anti " + dna + " §8Gene");
									}else if(virusList.contains(dna)){
										//Virus'
										if(vaccine == true)
											filledSyringe = setItem(Material.TRIPWIRE_HOOK, "§3Filled Syringe", "§8Left/Right-Click to inject", "§8Contains: " + dna + " §8Vaccine");
										else
											filledSyringe = setItem(Material.TRIPWIRE_HOOK, "§3Filled Syringe", "§8Left/Right-Click to inject", "§8Contains: " + dna + " §8Virus");
									}else
										filledSyringe = setItem(Material.TRIPWIRE_HOOK, "§3Filled Syringe", "§8Left/Right-Click to inject", "§8Contains: " + dna + " §8Gene");
									int amount = dest.getAmount() - 1;
									if(amount <= 0){
										event.setCurrentItem(filledSyringe);
									}else{
										event.getInventory().addItem(filledSyringe);
										dest.setAmount(amount);
									}
									player.setItemOnCursor(new ItemStack(Material.GLASS_BOTTLE, 1));
									player.updateInventory();
								}else
									System.out.println("DNA not found!");
							}else if((dest.getType().equals(Material.GLASS_BOTTLE)) && (sourceName.equals("§3Filled Syringe"))){
								String dna = null;
								for(String loreLine : source.getItemMeta().getLore()){
									for(String dnaType : dnaTypesList){
										if(loreLine.contains(dnaType)){
											dna = dnaType;
										}
									}
								}
								if(dna != null){
									ItemStack filledVial = new ItemStack(Material.POTION, 1, (short)8201);
									ItemMeta poisionMeta = filledVial.getItemMeta();
									ArrayList<String> bloodLoreList = new ArrayList<String>();
									poisionMeta.setDisplayName("§3Vial of blood");
									bloodLoreList.add("§8Blood mixed with " + dna + "§8 DNA");
									poisionMeta.setLore(bloodLoreList);
									filledVial.setItemMeta(poisionMeta);
									EnchantGlow.addGlow(filledVial);
									removeAttributes(filledVial);
									int amount = dest.getAmount() - 1;
									if(amount <= 0){
										event.setCurrentItem(filledVial);
									}else{
										dest.setAmount(amount);
										event.getInventory().addItem(filledVial);
									}
									int amount2 = source.getAmount() - 1;
									if(amount2 <= 0){
										player.setItemOnCursor(setItem(Material.TRIPWIRE_HOOK, "§3Syringe", "§8Left/Right-Click to get blood", "§8You must have a glass bottle"));
									}else{
										source.setAmount(amount2);
									}
									player.updateInventory();
								}
							}
						}
					}
				}
				if(event.getInventory().getType().equals(InventoryType.DISPENSER)){
					Dispenser d = (Dispenser) event.getView().getTopInventory().getHolder();
					Block block = d.getBlock();
					if (isExtractor(block)){
						if((rawSlot <= 2) || (rawSlot == 4) || (rawSlot == 6) || (rawSlot == 8)){
							event.setCancelled(true);
						}
					}
				}else if(event.getInventory().getType().equals(InventoryType.BREWING)){
					Inventory inv = event.getInventory();
					Block block = player.getTargetBlock((HashSet<Byte>)null, 5);
					//Has At Least 1 Blood Jar
					if ((inv.getItem(0) != null) || (inv.getItem(1) != null) || (inv.getItem(2) != null)){
						//Player IS Putting String Into Top Slot Of Centrifuge
						if ((event.getCursor().getType().equals(Material.STRING)) && isCentrifuge(block) && (event.getRawSlot()==3)){
							ItemStack item = event.getCursor();
							//Check if item is DNA
							if((item.getItemMeta() != null)  && (item.getItemMeta().getDisplayName() != null) && (!item.getType().equals(Material.AIR))){
								//Get DNA Type
								String dnaType = null;
								boolean anti = false;
								String type = "DNA";
								for(String v : virusList)
									if(item.getItemMeta().getDisplayName().contains(v))
										if(item.getItemMeta().getDisplayName().contains("vaccine")){
											dnaType = v;
											type = "vaccine";
										}else{
											dnaType = v;
											type = "virus";
										}
								if(item.getItemMeta().getDisplayName().equals("§dAnti-DNA")){
									//Is Anti DNA
									anti = true;
									for(String tmpDnaType : dnaTypesList)
										for(String s : item.getItemMeta().getLore())
											if(s.contains(tmpDnaType))
												dnaType = tmpDnaType;
								}else if(dnaType == null){
									for(String tmpDnaType : dnaTypesList){
										if(item.getItemMeta().getDisplayName().equals("§3" + tmpDnaType + " DNA")){
											ItemMeta meta = item.getItemMeta();
											ArrayList<String> loreList = (ArrayList<String>) meta.getLore();
											for(String loreLine : loreList){
												for(int num=0;num<=10;num++){
													if((loreLine != null) && (loreLine.equalsIgnoreCase("§8Bred " + num + " times"))){
														if(num==10){
															dnaType = tmpDnaType;
														}
													}
												}
											}
										}
									}
								}
								if(dnaType != null){
									//Check first slot (0)
									boolean slotZero = false;
									if((inv.getItem(0) != null) && (inv.getItem(0).getType().equals(Material.POTION))){
										if(inv.getItem(0).getItemMeta().getDisplayName().equals("§3Vial of blood")){
											slotZero = true;
										}
									}
									//Check second slot (1)
									boolean slotOne = false;
									if((inv.getItem(1) != null) && (inv.getItem(1).getType().equals(Material.POTION))){
										if(inv.getItem(1).getItemMeta().getDisplayName().equals("§3Vial of blood")){
											slotOne = true;
										}
									}
									//Check third slot (2)
									boolean slotTwo = false;
									if((inv.getItem(2) != null) && (inv.getItem(2).getType().equals(Material.POTION))){
										if(inv.getItem(2).getItemMeta().getDisplayName().equals("§3Vial of blood")){
											slotTwo = true;
										}
									}
									//Do Mix
									ItemStack failedBlood = new ItemStack(Material.GLASS_BOTTLE, 1);
									ItemStack succeededBlood = new ItemStack(Material.POTION, 1, (short)8201);
									ItemMeta poisionMeta = succeededBlood.getItemMeta();
									ArrayList<String> bloodLoreList = new ArrayList<String>();
									if(anti == true){
										poisionMeta.setDisplayName("§3Vial of anti DNA blood");
									}else
										poisionMeta.setDisplayName("§3Vial of blood");
									bloodLoreList.add("§8Blood mixed with " + dnaType + "§8 " + type);
									poisionMeta.setLore(bloodLoreList);
									succeededBlood.setItemMeta(poisionMeta);
									EnchantGlow.addGlow(succeededBlood);
									removeAttributes(succeededBlood);
									Random rand = new Random();
									//Do blood
									int min = 1;
									int max = getConfig().getInt("centrifugeChance");
									if(slotZero == true){
										int randomNum = rand.nextInt(max - min + 1) + min;
										if(randomNum == 1){
											inv.setItem(0, succeededBlood);
										}else{
											inv.setItem(0, failedBlood);
										}
									}
									if(slotOne == true){
										int randomNum = rand.nextInt(max - min + 1) + min;
										if(randomNum == 1){
											inv.setItem(1, succeededBlood);
										}else{
											inv.setItem(1, failedBlood);
										}
									}
									if(slotTwo == true){
										int randomNum = rand.nextInt(max - min + 1) + min;
										if(randomNum == 1){
											inv.setItem(2, succeededBlood);
										}else{
											inv.setItem(2, failedBlood);
										}
									}
									int amount = item.getAmount() - 1;
									if (amount <= 0){
										player.setItemOnCursor(null);
									}else{
										item.setAmount(amount);
									}
									player.updateInventory();
								}
							}
						}
					}
				}else if((event.getInventory().getType().equals(InventoryType.HOPPER) && (event.getInventory().getName().equals("Breeder")))){
					Inventory inv = event.getInventory();
					Block block = player.getTargetBlock((HashSet<Byte>)null, 5);
					if (isBreeder(block)){
						if((rawSlot == 1) || (rawSlot == 2) || (rawSlot == 3)){
							event.setCancelled(true);
							if(rawSlot == 2){
								breedDna(inv, player);
							}
						}
					}
				}
			}
		}
		
		 public static ItemStack removeAttributes(ItemStack item){
	        net.minecraft.server.v1_9_R2.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
	        NBTTagCompound tag;
	        if (!nmsStack.hasTag()){
	            tag = new NBTTagCompound();
	            nmsStack.setTag(tag);
	        }
	        else {
	            tag = nmsStack.getTag();
	        }
	        NBTTagList am = new NBTTagList();
	        tag.set("AttributeModifiers", am);
	        nmsStack.setTag(tag);
	        return CraftItemStack.asBukkitCopy(nmsStack);
	    }
		
		public void breedDna(Inventory inv ,Player p){
			if ((inv.getItem(0) != null) && (inv.getItem(4) != null) && (!inv.getItem(0).getType().equals(Material.AIR)) && (!inv.getItem(4).getType().equals(Material.AIR))){
				ItemStack fuel = inv.getItem(0);
				ItemStack breeder = inv.getItem(4);
				String breederName = breeder.getItemMeta().getDisplayName();
				//String In Right Slot
				//System.out.println("B1");
				if((breeder.getType().equals(Material.STRING)) && ((breederName.contains("§3")) || (breederName.contains("§c"))) && (breederName.contains("DNA")) && (breeder.getAmount()==1)){
					//Get Times Bred
					int bredTimes = 0;
					for(String loreLine : breeder.getItemMeta().getLore())
						if((loreLine.contains("§8Bred")) || (loreLine.contains("§4Bred")))
							for(int i=0;i<=10;i++)
								if(loreLine.contains(" " + i + " "))
									bredTimes = i;
					//System.out.println("B2 " + bredTimes);
					//Check Fuel Types
					if((fuel.getType().equals(Material.STRING)) && (bredTimes <=9)){
						//DNA
						try{
							if((fuel.getItemMeta().getDisplayName().contains("§3")) && (fuel.getItemMeta().getDisplayName().contains("DNA"))){
								//Breed DNA Strand
								int newNum = bredTimes + 1;
								ItemStack newDna = setItem(Material.STRING, breederName, breeder.getItemMeta().getLore().get(0), "§8Bred " + newNum + " times");
								inv.setItem(4, newDna);
								takeFuel(inv, fuel);
							}
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.ENDER_PEARL)) && (bredTimes == 10)){
						//System.out.println("B3");
						//Anti Cell
						try{
							if(fuel.getItemMeta().getDisplayName().contains("§dAnti-Cell")){
								//System.out.println("B4");
								//Vaccine Check
								for(String v : virusList)
									if(breeder.getItemMeta().getDisplayName().contains(v)){
										//Vaccine
										ItemStack newDna = setItem(Material.STRING, "§7DNA infused with " + v + " virus vaccine", "§8Bred 10 times");
										inv.setItem(4, newDna);
										takeFuel(inv, fuel);
										return;
									}
								//Not Vaccine
								ItemStack newDna = setItem(Material.STRING, "§dAnti-DNA", breeder.getItemMeta().getLore().get(0).replace("§8", "§5"), "§5Bred 10 times");
								inv.setItem(4, newDna);
								takeFuel(inv, fuel);
							}
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.ROTTEN_FLESH)) && (bredTimes == 10)){
						//Flu Disease
						try{
							ItemStack newDna = setItem(Material.STRING, "§cDNA infused with Flu virus", "§4Bred 10 times");
							inv.setItem(4, newDna);
							takeFuel(inv, fuel);
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.FERMENTED_SPIDER_EYE)) && (bredTimes == 10) && (breeder.getItemMeta().getDisplayName().toLowerCase().contains("poison"))){
						//Black Death
						try{
							ItemStack newDna = setItem(Material.STRING, "§cDNA infused with Black Death virus", "§4Bred 10 times");
							inv.setItem(4, newDna);
							takeFuel(inv, fuel);
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.SUGAR)) && (bredTimes == 10) && (breeder.getItemMeta().getDisplayName().toLowerCase().contains("undead"))){
						//Ebola
						try{
							ItemStack newDna = setItem(Material.STRING, "§cDNA infused with Ebola virus", "§4Bred 10 times");
							inv.setItem(4, newDna);
							takeFuel(inv, fuel);
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.EYE_OF_ENDER)) && (bredTimes == 10) && (breeder.getItemMeta().getDisplayName().toLowerCase().contains("burn"))){
						//Anthrax
						try{
							ItemStack newDna = setItem(Material.STRING, "§cDNA infused with Anthrax virus", "§4Bred 10 times");
							inv.setItem(4, newDna);
							takeFuel(inv, fuel);
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.SKULL_ITEM)) && (fuel.getData().equals(1)) && (bredTimes == 10) && (breeder.getItemMeta().getDisplayName().toLowerCase().contains("wither"))){
						//Scarlet Fever
						try{
							ItemStack newDna = setItem(Material.STRING, "§cDNA infused with Scarlet Fever virus", "§4Bred 10 times");
							inv.setItem(4, newDna);
							takeFuel(inv, fuel);
						}catch(Exception x){x.printStackTrace();}
					}else if ((fuel.getType().equals(Material.DEAD_BUSH)) && (bredTimes == 10) && (breeder.getItemMeta().getDisplayName().toLowerCase().contains("end"))){
						//SmallPox
						try{
							ItemStack newDna = setItem(Material.STRING, "§cDNA infused with SmallPox virus", "§4Bred 10 times");
							inv.setItem(4, newDna);
							takeFuel(inv, fuel);
						}catch(Exception x){x.printStackTrace();}
					}
				}
			}
		}
		
		public void takeFuel(Inventory inv, ItemStack fuel){
			int amount = fuel.getAmount() - 1;
			if(amount <= 0){
				inv.setItem(0, null);
			}else{
				inv.getItem(0).setAmount(amount);
			}
		}
		
		@EventHandler(priority=EventPriority.HIGH)
		public void onInvClose(InventoryCloseEvent event){
			Player player = (Player) event.getPlayer();
			World world = player.getWorld();
			//System.out.println("ic1"); //------DEBUG------
			Inventory inv = event.getInventory();
			//System.out.println("ic2"); //------DEBUG------
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
				if(inv.getName().equals("Centrifuge")){
					//System.out.println("ic3"); //------DEBUG------
					for(ItemStack item : inv.getContents()){
						if(item != null){
							player.getWorld().dropItemNaturally(player.getLocation(), item);
						}
					}
				}else if(inv.getName().equals("Breeder")){
					//System.out.println("ic4"); //------DEBUG------
					for(ItemStack item : inv.getContents()){
						if((item != null) && (!item.getItemMeta().getDisplayName().equals("§3Click to breed")) && (!item.getItemMeta().getDisplayName().equals("§8"))){
							player.getWorld().dropItemNaturally(player.getLocation(), item);
						}
					}
				}
			}
		}
		
		@SuppressWarnings("deprecation")
		@EventHandler(priority=EventPriority.HIGH)
		public void onBlockPlace(BlockPlaceEvent event) {
			Player player = event.getPlayer();
			World world = player.getWorld();
			if (event.getPlayer().getItemInHand() != null){
				ItemStack item = event.getPlayer().getItemInHand();
				if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
					if(item.getType().equals(Material.CARPET)){
						Block carpet = event.getBlock();
						if(carpet.getData() == 4){
							Location tank = carpet.getLocation();
							tank.setY(tank.getY() - 1);
							Block block = tank.getBlock();
							if((block.getType().equals(Material.STAINED_GLASS)) && (block.getData() == 0) && (isCentrifuge(block))){
								Location centLock = block.getLocation();
								if (isSave(world, centLock, "centrifugeList") == false) {
									addSave(world, centLock, "centrifugeList");
									if(displayingParticles(centLock) == false)
										showCentrifugeParticles(centLock);
								}
							}
						}
					}else if(item.getType().equals(Material.GOLD_BLOCK)){
						Block gold = event.getBlock();
						Location tank = gold.getLocation();
						tank.setY(tank.getY() + 1);
						Block block = tank.getBlock();
						if(isCentrifuge(block)){
							Location centLock = block.getLocation();
							if (isSave(world, centLock, "centrifugeList") == false) {
								addSave(world, centLock, "centrifugeList");
								if(displayingParticles(centLock) == false)
								showCentrifugeParticles(centLock);
							}
						}
					}if((item.getType().equals(Material.QUARTZ_BLOCK)) && (item.getData().getData() == 2)){
						//Placed quartz pillar
						Block pillar = event.getBlock();
						Location tank = pillar.getLocation();
						tank.setY(tank.getY() + 1);
						Block block = tank.getBlock();
						if((block.getType().equals(Material.STAINED_GLASS)) && (block.getData() == 5) && (isBreeder(block))){
							Location breederLock = block.getLocation();
							if (isSave(world, breederLock, "breederList") == false) {
								addSave(world, breederLock, "breederList");
								if(displayingParticles(breederLock) == false)
									showBreederParticles(breederLock);
							}
						}
					}else if((item.getTypeId() == 44) && (item.getData().getData() == 7)){
						//Placed slab
						Block slab = event.getBlock();
						Location tank = slab.getLocation();
						tank.setY(tank.getY() - 1);
						Block block = tank.getBlock();
						if((block.getType().equals(Material.STAINED_GLASS)) && (block.getData() == 5) && (isBreeder(block))){
							Location breederLock = block.getLocation();
							if (isSave(world, breederLock, "breederList") == false) {
								addSave(world, breederLock, "breederList");
								if(displayingParticles(breederLock) == false)
									showBreederParticles(breederLock);
							}
						}
					}else if((item.getType().equals(Material.STAINED_GLASS)) && (item.getData().getData() == 5)){
						//Placed slab
						Block block = event.getBlock();
						if(isBreeder(block)){
							Location breederLock = block.getLocation();
							if (isSave(world, breederLock, "breederList") == false) {
								addSave(world, breederLock, "breederList");
								if(displayingParticles(breederLock) == false)
									showBreederParticles(breederLock);
							}
						}
					}else if(item.getItemMeta().getLore() != null){
						if(isValid(item) == false){
							player.sendMessage("§cYou can't place this!");
							event.setCancelled(true);
						}
					}
				}
			}
		}
		
		public String getCoordName(Location loc){
	    	double x = loc.getX();
	    	double y = loc.getY();
	    	double z = loc.getZ();
	    	String coordName = (x + "." + y + "." + z);
	    	coordName = coordName.replace(".", "");
			return coordName;
		}
		
		public void showCentrifugeParticles(final Location loc){
			World world = loc.getWorld();

			if((getConfig().getBoolean("enableParticles") == true) && (loc.getBlock().getType().equals(Material.STAINED_GLASS)) && (isSave(world, loc, "centrifugeList") == true) && (isCentrifuge(loc.getBlock()) == true)){
				Location pS = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
				pS.setX(pS.getX()+0.5);
				pS.setZ(pS.getZ()+0.5);
				//loc.getWorld().playEffect(loc, Effect.MOBSPAWNER_FLAMES, 79);
				//Original Buggy
				//ParticleEffects.sendToLocation(ParticleEffects.HEART, pS, 0, 0, 0, 1, 1);
				//ParticleEffects.sendToLocation(ParticleEffects.CLOUD, pS, 0, 0, 0, 0, 25);
				//Multi World Fix
				for(Player p : world.getPlayers()){
					ParticleEffects.sendToPlayer(ParticleEffects.HEARTS, p, pS, 0, 0, 0, 1, 1);
					ParticleEffects.sendToPlayer(ParticleEffects.CLOUD, p, pS, 0, 0, 0, 0, 25);
				}
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				     public void run() {
						showCentrifugeParticles(loc);
					 }
				}, (1 * 20));
			}else if(isSave(world, loc, "centrifugeList") == true){
				removeSave(world, loc, "centrifugeList");
				machineBlock mb = new machineBlock(loc, getCoordName(loc), true);
				machineBlockList.remove(mb);
			}
		}
		
		public void showBreederParticles(final Location loc){
			//System.out.println("Show Breeder Part"); //------DEBUG------
			World world = loc.getWorld();
			if((getConfig().getBoolean("enableParticles") == true) && (loc.getBlock().getType().equals(Material.STAINED_GLASS)) && (isSave(world, loc, "breederList") == true) && (isBreeder(loc.getBlock()) == true)){
				//System.out.println("SBP 1"); //------DEBUG------
				Location pS = new Location(loc.getWorld(), loc.getX(), loc.getY(), loc.getZ());
				pS.setX(pS.getX()+0.6);
				pS.setZ(pS.getZ()+0.6);
				pS.setY(pS.getY()+0.4);
				//Original Buggy
				//ParticleEffects.sendToLocation(ParticleEffects.ENCHANTMENT_TABLE, pS, 0, 0, 0, 1, 60);
				//ParticleEffects.sendToLocation(ParticleEffects.GREEN_SPARKLE, pS, 0, 0, 0, 3, 10);
				//Multi World Fix
				for(Player p : world.getPlayers()){
					ParticleEffects.sendToPlayer(ParticleEffects.ENCHANTS, p, pS, 0, 0, 0, 1, 60);
					ParticleEffects.sendToPlayer(ParticleEffects.HAPPY, p, pS, 0, 0, 0, 3, 10);
				}
				Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
				     public void run() {
						showBreederParticles(loc);
					 }
				}, (1 * 20));
			}else if(isSave(world, loc, "breederList") == true){
				removeSave(world, loc, "breederList");
				if(getMachineBlock(getCoordName(loc)) != null)
					return;
				machineBlock mb = new machineBlock(loc, getCoordName(loc), true);
				machineBlockList.remove(mb);
			}
		}
		
		public boolean isValid(ItemStack item){
			for(String mob : mobList){
				//System.out.println("Name = " + item.getItemMeta().getDisplayName()); //------DEBUG------
				if(item.getItemMeta().getDisplayName().equals("§3" + mob + " Skin")){
					return false;
				}else if(item.getItemMeta().getDisplayName().equals("§3" + mob + " Cell")){
					return false;
				}
			}
			for(String dnaType : dnaTypesList){
				if(item.getItemMeta().getDisplayName().contains("§3" + dnaType)){
					return false;
				}
			}
			if(item.getItemMeta().getDisplayName().equals("§3Syringe")){
				return false;
			}
			//System.out.println("Is Valid"); //------DEBUG------
			return true;
		}
		
		@SuppressWarnings("deprecation")
		public boolean isAnalyzer(Block furnace){
			Location tankLoc = furnace.getLocation().clone();
			Location lidLoc = furnace.getLocation().clone();
			tankLoc.setY(tankLoc.getY() + 1);
			lidLoc.setY(lidLoc.getY() + 2);
			Block tank = tankLoc.getBlock();
			Block lid = lidLoc.getBlock();
			//System.out.println("Tank: " + tank.getTypeId());
			//System.out.println("Tank Data: " + tank.getData());
			//System.out.println("Lid: " + lid.getTypeId());
			if ((tank.getTypeId() == 95) && (tank.getData() == 3) && (lid.getTypeId() == 44)){
				return true;
			}
			return false;
		}
		
		@SuppressWarnings("deprecation")
		public boolean isExtractor(Block dispenser){
			Location tankLoc = dispenser.getLocation();
			Location lidLoc = dispenser.getLocation();
			tankLoc.setY(tankLoc.getY() + 1);
			lidLoc.setY(lidLoc.getY() + 2);
			Block tank = tankLoc.getBlock();
			Block lid = lidLoc.getBlock();
			//System.out.println("Tank: " + tank.getTypeId());
			//System.out.println("Data: " + tank.getData());
			//System.out.println("Lid: " + lid.getType());
			if ((tank.getTypeId() == 95) && (tank.getData() == 14) && (lid.getType().equals(Material.DAYLIGHT_DETECTOR))){
				//System.out.println("True");
				return true;
			}
			return false;
		}
		
		@SuppressWarnings("deprecation")
		public boolean isCentrifuge(Block tank){
			Location baseLoc = tank.getLocation();
			Location lidLoc = tank.getLocation();
			baseLoc.setY(baseLoc.getY() - 1);
			lidLoc.setY(lidLoc.getY() + 1);
			Block base = baseLoc.getBlock();
			Block lid = lidLoc.getBlock();
			if ((base.getType().equals(Material.GOLD_BLOCK)) && (lid.getType().equals(Material.CARPET)) && (lid.getData() == 4) ){
				return true;
			}
			return false;
		}
		
		@SuppressWarnings("deprecation")
		public boolean isBreeder(Block tank){
			Location baseLoc = tank.getLocation();
			Location lidLoc = tank.getLocation();
			baseLoc.setY(baseLoc.getY() - 1);
			lidLoc.setY(lidLoc.getY() + 1);
			Block base = baseLoc.getBlock();
			Block lid = lidLoc.getBlock();
			if ((base.getType().equals(Material.QUARTZ_BLOCK)) && (base.getData() == 2) && (lid.getTypeId() == 44) && (lid.getData() == 7)){
				return true;
			}
			return false;
		}
		
	    @SuppressWarnings("deprecation")
		@EventHandler(priority=EventPriority.HIGH)
		public void onInvOpen(InventoryOpenEvent event) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException{
	    	World world = event.getPlayer().getWorld();
			if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
		    	//------------ Analyzer --------------
				if ((event.getInventory().getType() != null) && (event.getInventory().getType().equals(InventoryType.FURNACE))){
					try{
						Inventory old = event.getInventory();
						Block block = null;
						if(old.getHolder() instanceof Furnace)
							block = ((Furnace)old.getHolder()).getBlock();
						if((block != null) && (isAnalyzer(block))){
							CraftFurnace chest = (CraftFurnace) block.getState();
							try{
							    Field inventoryField = chest.getClass().getDeclaredField("furnace");
							    inventoryField.setAccessible(true);
							    TileEntityFurnace teChest = ((TileEntityFurnace) inventoryField.get(chest));
							    teChest.a("Analyzer");
							}catch (Exception e){e.printStackTrace();}
						}
					}catch(Exception x){x.printStackTrace();}
				}
		    	//------------ Extractor --------------
				if ((event.getInventory().getType() != null) && (event.getInventory().getType().equals(InventoryType.DISPENSER))){
					//System.out.println("d1");
					Inventory old = event.getInventory();
					Block block = null;
					if(old.getHolder() instanceof Dispenser)
						block = ((Dispenser)old.getHolder()).getBlock();
					//System.out.println("d2");
					if((block != null) && (isExtractor(block))){
						//System.out.println("d3");
						CraftDispenser d = (CraftDispenser) block.getState();
						ItemStack nothing = new ItemStack(160);
						ItemMeta meta = nothing.getItemMeta();
						meta.setDisplayName("§8");
						nothing.setItemMeta(meta);
						ItemStack arrow = setItem(Material.IRON_PLATE, "§8<-In|Out->", "§8  V Fuel V");
						d.getInventory().setItem(0, nothing);
						d.getInventory().setItem(1, nothing);
						d.getInventory().setItem(2, nothing);
						d.getInventory().setItem(4, arrow);
						d.getInventory().setItem(6, nothing);
						//d.getInventory().setItem(7, nothing);
						d.getInventory().setItem(8, nothing);
						d.update();
						//System.out.println("d4");
						try{
						    Field inventoryField = d.getClass().getDeclaredField("dispenser");
						    inventoryField.setAccessible(true);
						    TileEntityDispenser teChest = ((TileEntityDispenser) inventoryField.get(d));
						    teChest.a("Extractor");
						}catch (Exception e){e.printStackTrace();}
						//System.out.println("d5");
					}
				}
		    	//------------ Breeder --------------
				if ((event.getInventory().getType() != null) && (event.getInventory().getType().equals(InventoryType.HOPPER))){
					//System.out.println("Is a hopper"); //------DEBUG------
					Inventory inv = event.getInventory();
					if (inv.getName().equals("Breeder")){
						ItemStack nothing = new ItemStack(351, 1, (short) 10);
						ItemMeta meta = nothing.getItemMeta();
						meta.setDisplayName("§8");
						nothing.setItemMeta(meta);
						ItemStack icon = setItem(Material.SLIME_BALL, "§3Click to breed", "§8<-fuel DNA", "§8Breeding DNA->");
						inv.setItem(1, nothing);
						inv.setItem(2, icon);
						inv.setItem(3, nothing);
						//((Player) event.getPlayer()).updateInventory();
					}
				}
			}
		}
		
	    @SuppressWarnings({ "deprecation", "unchecked" })
		@EventHandler(priority=EventPriority.HIGH)
	    public void onPlayerAttack(EntityDamageByEntityEvent event){
	    	//System.out.println("AG: damage a " + event.getEntity().getType().getName()); //------DEBUG------
	    	Entity attacker = null;
	    	Entity victim = null;
	    	if (event.getDamager() != null){
	    		attacker = event.getDamager();
	    	}else{
	    		return;
	    	}
	    	if (event.getEntity() != null){
		    	victim = event.getEntity();
	    	}else{
	    		return;
	    	}
			if ((attacker.getType().equals(EntityType.PLAYER)) && (!victim.getType().equals(EntityType.PLAYER))){
				Player player = (Player) attacker;
				//player.sendMessage("You attacked a " + victim.getType());
				World world = player.getWorld();
				Entity mob = victim;
				ItemStack drop = null;
				if ((getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))) {
					//Get DNA Scraper Item
					ItemStack dnaScraper = setItem(Material.IRON_SPADE, "§3Skin Scraper", "§8Left-Click mob to get Skin");
					dnaScraper.setDurability((short) player.getItemInHand().getDurability());
					//Get Syringe Item
					ItemStack syringe = setItem(Material.TRIPWIRE_HOOK, "§3Syringe", "§8Left/Right-Click to get blood", "§8You must have a glass bottle");
					syringe.setAmount(player.getItemInHand().getAmount());
					//Check if Player is using DNA Scraper else check Syringe
					if(player.getItemInHand().equals(dnaScraper)){
						for(String mob2 : mobList){
							if(mob2.equals("WitherSkeleton")){
								if(mob.getType().equals(EntityType.SKELETON)){
									Skeleton sk = (Skeleton) mob;
									if(sk.getType().equals(SkeletonType.WITHER)){
										drop = setItem(Material.PUMPKIN_SEEDS, "§3" + mob2 + " Skin", "§8Skin scraped off a " + mob2);
									}
								}
							}else if(mob2.equals("Horse")){
								if(mob.getType().equals(EntityType.HORSE)){
									drop = setItem(Material.PUMPKIN_SEEDS, "§3" + mob2 + " Skin", "§8Skin scraped off a " + mob2);
								}
							}else if(mob.getType().equals(EntityType.fromName(mob2))){
								drop = setItem(Material.PUMPKIN_SEEDS, "§3" + mob2 + " Skin", "§8Skin scraped off a " + mob2);
							}

						}
						if(drop != null){
							mob.getWorld().dropItemNaturally(mob.getLocation(), drop);
						}
					}else if(player.getItemInHand().equals(syringe)){
						ItemStack bottles = null;
						for(ItemStack posBot : player.getInventory()){
							if((posBot != null) && (posBot.getType().equals(Material.GLASS_BOTTLE))){
								bottles = posBot;
							}
						}
						if(bottles != null){
							for(String mob2 : mobList){
								if(mob2.equals("WitherSkeleton")){
									if(mob.getType().equals(EntityType.SKELETON)){
										Skeleton sk = (Skeleton) mob;
										if(sk.getType().equals(SkeletonType.WITHER)){
											drop = getBlood(mob2, false);
										}
									}
								}else if(mob2.equals("Horse")){
									if(mob.getType().equals(EntityType.HORSE)){
										drop = getBlood(mob2, false);
									}
								}else if(mob.getType().equals(EntityType.fromName(mob2))){
									drop = getBlood(mob2, false);
								}
							}
							if(drop != null){
								if((bottles.getAmount() - 1) <= 0){
									player.getInventory().removeItem(bottles);
								}else{
									bottles.setAmount(bottles.getAmount() - 1);
								}
								int syringeAmount = player.getItemInHand().getAmount() - 1;
								if(syringeAmount <= 0){
									player.getInventory().removeItem(player.getItemInHand());
								}else{
									player.getItemInHand().setAmount(syringeAmount);
								}
								player.getInventory().addItem(drop);
							}
						}
					}
				}
			}
			if ((attacker.getType().equals(EntityType.PLAYER)) && (victim.getType().equals(EntityType.PLAYER))){
				Player pAttacker = (Player) attacker;
				Player pVictim = (Player) victim;
				World world = pAttacker.getWorld();
				if ((pAttacker.getItemInHand() != null) && (getConfig().getList("enabledworlds").contains(world.getName())) || (getConfig().getList("enabledworlds").contains("<all>"))){
					//Get Syringe Item
					ItemStack syringe = setItem(Material.TRIPWIRE_HOOK, "§3Syringe", "§8Left/Right-Click to get blood", "§8You must have a glass bottle");
					syringe.setAmount(pAttacker.getItemInHand().getAmount());
					//Check if attacker has Syringe
					if(pAttacker.getItemInHand().equals(syringe)){
						ItemStack bottles = null;
						for(ItemStack posBot : pAttacker.getInventory()){
							if((posBot != null) && (posBot.getType().equals(Material.GLASS_BOTTLE))){
								bottles = posBot;
							}
						}
						if(bottles != null){
							ItemStack drop = getBlood(pVictim.getDisplayName(), true);
							if((bottles.getAmount() - 1) <= 0){
								pAttacker.getInventory().removeItem(bottles);
							}else{
								bottles.setAmount(bottles.getAmount() - 1);
							}
							int syringeAmount = pAttacker.getItemInHand().getAmount() - 1;
							if(syringeAmount <= 0){
								pAttacker.getInventory().removeItem(pAttacker.getItemInHand());
							}else{
								pAttacker.getItemInHand().setAmount(syringeAmount);
							}
							pAttacker.getInventory().addItem(drop);
							((LivingEntity) pVictim).addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 250, 1), true);
						}
					}else if((pAttacker.getItemInHand().getItemMeta() != null) && (pAttacker.getItemInHand().getItemMeta().getDisplayName() != null) && (pAttacker.getItemInHand().getItemMeta().getDisplayName().equals("§3Filled Syringe"))){
						inject(pAttacker, pVictim);
					}
				}
			}
			if ((attacker instanceof Player)) {
				Player pAttacker = (Player) attacker;
				ArrayList<String> abilityList = new ArrayList<String>();
		    	if(getSaves().getList("playerList." + pAttacker.getName()) != null){
		    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + pAttacker.getName());
			    	if((abilityList.contains("Poison")) && (getConfig().getBoolean("Poison") == true)){
						((LivingEntity) victim).addPotionEffect(new PotionEffect(PotionEffectType.POISON, 100, 1), true);
			    	}
			    	if((abilityList.contains("Wither")) && (getConfig().getBoolean("Wither") == true)){
						((LivingEntity) victim).addPotionEffect(new PotionEffect(PotionEffectType.WITHER, 100, 1), true);
			    	}
		    	}	
			}
	    }
	    
	    @SuppressWarnings("unchecked")
		public void addDna(final Player victom, Player attacker, String dna){
	    	ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + victom.getName()) != null){
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + victom.getName());
	    	}
	    	if((!abilityList.contains(dna)) && (!dna.equals("Common"))){
		    	abilityList.add(dna);
		    	getSaves().set("playerList." + victom.getName(), abilityList);
	    		saveSaves();
	    		if(attacker != null)
	    			attacker.sendMessage("§7You have successfully injected " + dna + " §7DNA into " + victom.getDisplayName() + "§7.");
	    	}else if(dna.equals("Common")){
	    		if(attacker != null)
	    			attacker.sendMessage("§7You have successfully injected " + dna + " §7DNA into " + victom.getDisplayName() + "§7.");
	    	}else{
	    		if(attacker != null)
	    			attacker.sendMessage(victom.getDisplayName() + " §7allredy has the " + dna + " §7DNA.");
	    	}
	    	if((dna.equals("Health")) && (getConfig().getBoolean("Health") == true)){
	    		setHearts(victom);
	    	}
			if ((dna.equals("Speed")) && (getConfig().getBoolean("Speed") == true)){
				//System.out.println("LastY = " + userGet(victom).getLastY); //------DEBUG------
				if(userGet(victom).getLastY != 1){
					//System.out.println("Speed3"); //------DEBUG------
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
					     public void run() {
						    victom.setWalkSpeed(victom.getWalkSpeed()*getConfig().getInt("speedSpeed"));
						 }
					}, (1 * 20));
					userGet(victom).setUserY(1);
				}
			}
	    }
	    
	    @SuppressWarnings("unchecked")
		public void removeDna(final Player victom, Player attacker, String dna){
	    	ArrayList<String> abilityList = new ArrayList<String>();
	    	if(getSaves().getList("playerList." + victom.getName()) != null)
	    		abilityList = (ArrayList<String>) getSaves().getList("playerList." + victom.getName());
	    	if((abilityList.contains(dna)) && (!dna.equals("Common"))){
		    	abilityList.remove(dna);
		    	getSaves().set("playerList." + victom.getName(), abilityList);
	    		saveSaves();
	    		if(attacker != null)
	    			attacker.sendMessage("§7You have successfully injected anti " + dna + " §7DNA into " + victom.getDisplayName() + "§7.");
	    	}else if(dna.equals("Common")){
	    		if(attacker != null)
	    			attacker.sendMessage("§7You have successfully injected anti " + dna + " §7DNA into " + victom.getDisplayName() + "§7.");
	    	}else{
	    		if(attacker != null)
	    			attacker.sendMessage(victom.getDisplayName() + " §7doesn't have the " + dna + " §7DNA.");
	    	}
	    	if((dna.equals("Health")) && (getConfig().getBoolean("Health") == true)){
	    		takeHearts(victom);
	    	}
			if ((dna.equals("Speed")) && (getConfig().getBoolean("Speed") == true)){
				if(userGet(victom).getLastY != 1){
					Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
					     public void run() {
						    victom.setWalkSpeed(1);
						 }
					}, (1 * 20));
					userGet(victom).setUserY(1);
				}
			}
	    }
	    
	    @EventHandler
	    public void restrictCrafting(PrepareItemCraftEvent e) {
	    	CraftingInventory ci = e.getInventory();
	    	//Needed = ItemStack cell = setItem(Material.SNOW_BALL, "§3" + mob + " Cell", "§8Cell from a " + mob);
	    	//Geting = ItemStack antiCell = setItem(Material.ENDER_PEARL, "§dAnti-Cell", "§5For DNA removal");
		    if ((ci.getResult().getItemMeta() != null) && (ci.getResult().getItemMeta().getLore() != null) && (ci.getResult().getItemMeta().getLore().contains("§5For DNA removal"))) {
		        // this is our custom item - make sure all three ingredients are found
		        boolean found = false;
		        for (ItemStack item : ci.getMatrix()) {
		        	if ((item != null) && (item.getItemMeta() != null)  && (item.getItemMeta().getLore() != null)) {
		        		for(String s : item.getItemMeta().getLore())
			        		if (s.contains("§8Cell from a "))
			        			found = true;
		        	}
		        }
		        if (found == false){
		        	// one or more missing items
		        	ci.setResult(null);
		        }
		    }else  if (ci.getResult().getType().equals(Material.EYE_OF_ENDER)){
		    	//Anti Cell Use Check
		        boolean found = false;
		        for (ItemStack item : ci.getMatrix()) {
		        	if ((item != null) && (item.getItemMeta() != null)  && (item.getItemMeta().getLore() != null)) {
		        		for(String s : item.getItemMeta().getLore())
			        		if (s.contains("§5For DNA removal"))
			        			found = true;
		        	}
		        }
		        if (found == true){
		        	//Is Anti Cell
		        	ci.setResult(null);
		        }
		    }
	    }
		
		public void addRecipes(){
			//-------------DNA SCRAPER--------------
			ItemStack dnaScraper = setItem(Material.IRON_SPADE, "§3Skin Scraper", "§8Left-Click mob to get Skin");
			dnaScraper.setDurability((short) 150);
			ShapedRecipe dnaScraperRecipe = new ShapedRecipe(dnaScraper).shape("002", "010", "100");
			dnaScraperRecipe.setIngredient('1', Material.STICK);
			dnaScraperRecipe.setIngredient('2', Material.IRON_BLOCK);
			getServer().addRecipe(dnaScraperRecipe);
			//-------------Syringe--------------
			ItemStack syringe = setItem(Material.TRIPWIRE_HOOK, "§3Syringe", "§8Left/Right-Click to get blood");
			ItemMeta meta = syringe.getItemMeta();
			ArrayList<String> syringeLoreList = new ArrayList<String>();
			syringeLoreList.add("§8Left/Right-Click to get blood");
			syringeLoreList.add("§8You must have a glass bottle");
			meta.setLore(syringeLoreList);
			syringe.setItemMeta(meta);
			ShapedRecipe syringeRecipe = new ShapedRecipe(syringe).shape("001", "020", "300");
			syringeRecipe.setIngredient('1', Material.IRON_BLOCK);
			syringeRecipe.setIngredient('2', Material.GLASS);
			syringeRecipe.setIngredient('3', Material.BLAZE_ROD);
			getServer().addRecipe(syringeRecipe);
			//-------------Anti Cell-------------
			//ItemStack cell = setItem(Material.SNOW_BALL, "§3" + mob + " Cell", "§8Cell from a " + mob);
			ItemStack antiCell = setItem(Material.ENDER_PEARL, "§dAnti-Cell", "§5For DNA removal");
			ShapedRecipe cellRecipe = new ShapedRecipe(antiCell).shape("222", "212", "222");
			cellRecipe.setIngredient('1', Material.SNOW_BALL);
			cellRecipe.setIngredient('2', Material.REDSTONE);
			getServer().addRecipe(cellRecipe);
		}
		
		public void addMobs(ArrayList<String> list){
			list.add("Bat");
			list.add("Blaze");
			list.add("CaveSpider");
			list.add("Chicken");
			list.add("Cow");
			list.add("Creeper");
			list.add("EnderMan");
			list.add("Ghast");
			list.add("Horse");
			list.add("Spider");
			list.add("VillagerGolem");
			list.add("Ozelot");
			list.add("Pig");
			list.add("Sheep");
			list.add("Skeleton");
			list.add("Squid");
			list.add("Zombie");
			//New
			list.add("Villager");
			list.add("Wolf");
			list.add("PigZombie");
			list.add("SilverFish");
			list.add("LavaSlime");
			list.add("Slime");
			list.add("Witch");
		}
		
		public void addVirusTypes(ArrayList<String> list){
			list.add("Flu");
			list.add("Black Death");
			list.add("Ebola");
			list.add("Anthrax");
			list.add("Scarlet Fever");
			list.add("SmallPox");
		}
		
		public void addDnaTypes(ArrayList<String> list){
			list.add("Flying");
			list.add("FireBall");
			list.add("Poison");
			list.add("Climb");
			list.add("NoFall");
			list.add("Milk");
			list.add("EatGrass");
			list.add("Explode");
			list.add("Teleport");
			list.add("Jump");
			list.add("Health");
			list.add("Speed");
			list.add("FireProof");
			list.add("Woolly");
			list.add("Wither");
			list.add("DayBurn");
			list.add("Undead");
			list.add("Water");
			list.add("Common");
			//New
			list.add("Villager");
			list.add("Canine");
			list.add("PigMan");
			list.add("Stone");
			list.add("Slime");
			list.add("Potion");
			//Virus'
			list.addAll(virusList);
		}
		
	    @SuppressWarnings("deprecation")
		public void defineTransBlocks() {
	    	transBlocks.add(Integer.valueOf(Material.RAILS.getId()));
	    	transBlocks.add(Integer.valueOf(Material.ANVIL.getId()));
	    	transBlocks.add(Integer.valueOf(Material.BREWING_STAND.getId()));
	    	transBlocks.add(Integer.valueOf(Material.DAYLIGHT_DETECTOR.getId()));
	    	transBlocks.add(Integer.valueOf(Material.DETECTOR_RAIL.getId()));
	    	transBlocks.add(Integer.valueOf(Material.DEAD_BUSH.getId()));
	    	transBlocks.add(Integer.valueOf(Material.RED_MUSHROOM.getId()));
	    	transBlocks.add(Integer.valueOf(Material.LADDER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.RED_ROSE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.YELLOW_FLOWER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.BROWN_MUSHROOM.getId()));
	    	transBlocks.add(Integer.valueOf(6));
	    	transBlocks.add(Integer.valueOf(31));
	    	transBlocks.add(Integer.valueOf(75));
	    	transBlocks.add(Integer.valueOf(76));
	    	transBlocks.add(Integer.valueOf(104));
	    	transBlocks.add(Integer.valueOf(105));
	    	transBlocks.add(Integer.valueOf(111));
	    	transBlocks.add(Integer.valueOf(127));
	    	transBlocks.add(Integer.valueOf(132));
	    	transBlocks.add(Integer.valueOf(140));
	    	transBlocks.add(Integer.valueOf(141));
	    	transBlocks.add(Integer.valueOf(142));
	    	transBlocks.add(Integer.valueOf(149));
	    	transBlocks.add(Integer.valueOf(150));
	    	transBlocks.add(Integer.valueOf(171));
	    	transBlocks.add(Integer.valueOf(Material.PORTAL.getId()));
	    	transBlocks.add(Integer.valueOf(Material.POWERED_RAIL.getId()));
	    	transBlocks.add(Integer.valueOf(Material.WEB.getId()));
	    	transBlocks.add(Integer.valueOf(Material.TORCH.getId()));
	    	transBlocks.add(Integer.valueOf(Material.SIGN.getId()));
	    	transBlocks.add(Integer.valueOf(Material.STONE_BUTTON.getId()));
	    	transBlocks.add(Integer.valueOf(Material.STONE_PLATE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.WOOD_PLATE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.WOOD_BUTTON.getId()));
	    	transBlocks.add(Integer.valueOf(Material.SUGAR_CANE_BLOCK.getId()));
	    	transBlocks.add(Integer.valueOf(Material.GOLD_PLATE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.IRON_PLATE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.HOPPER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.LADDER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.VINE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.BED.getId()));
	    	transBlocks.add(Integer.valueOf(Material.BED_BLOCK.getId()));
	    	transBlocks.add(Integer.valueOf(Material.SNOW.getId()));
	    	transBlocks.add(Integer.valueOf(Material.RAILS.getId()));
	    	transBlocks.add(Integer.valueOf(Material.LEVER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.TRAP_DOOR.getId()));
	    	transBlocks.add(Integer.valueOf(Material.PISTON_EXTENSION.getId()));
	    	transBlocks.add(Integer.valueOf(Material.PISTON_MOVING_PIECE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.TRIPWIRE_HOOK.getId()));
	    	transBlocks.add(Integer.valueOf(93));
	    	transBlocks.add(Integer.valueOf(94));
	    	transBlocks.add(Integer.valueOf(Material.BOAT.getId()));
	    	transBlocks.add(Integer.valueOf(Material.MINECART.getId()));
	    	transBlocks.add(Integer.valueOf(Material.CAKE.getId()));
	    	transBlocks.add(Integer.valueOf(Material.CAKE_BLOCK.getId()));
	    	transBlocks.add(Integer.valueOf(Material.WATER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.STATIONARY_WATER.getId()));
	    	transBlocks.add(Integer.valueOf(Material.LAVA.getId()));
	    	transBlocks.add(Integer.valueOf(Material.STATIONARY_LAVA.getId()));
	      }
		
	    public ArrayList<Block> getVines(Player player)
	    {
	      if (this.vineMap.containsKey(player.getName())) {
	        return this.vineMap.get(player.getName());
	      }
	      ArrayList<Block> temp = new ArrayList<Block>();
	      return temp;
	    }

	    public void setVines(Player player, ArrayList<Block> vines)
	    {
	      this.vineMap.put(player.getName(), vines);
	    }

	    public void addVines(Player player, Block vine)
	    {
	      ArrayList<Block> updated = new ArrayList<Block>();
	      updated = getVines(player);
	      updated.add(vine);
	      setVines(player, updated);
	    }

	    public BlockFace yawToFace(float yaw) {
	      BlockFace[] axis = { BlockFace.SOUTH, BlockFace.WEST, BlockFace.NORTH, BlockFace.EAST };
	      return axis[(java.lang.Math.round(yaw / 90.0F) & 0x3)];
	    }

		public ItemStack setItem(Material mat, String dnaName, String dnaLore){
			ItemStack item = new ItemStack(mat, 1);
			ItemMeta meta = item.getItemMeta();
			ArrayList<String> skinLoreList = new ArrayList<String>();
			meta.setDisplayName(dnaName);
			skinLoreList.add(dnaLore);
			meta.setLore(skinLoreList);
			item.setItemMeta(meta);
			EnchantGlow.addGlow(item);
			return item;
		}
		
		public ItemStack setItem(Material mat, String dnaName, String dnaLore1, String dnaLore2){
			ItemStack item = new ItemStack(mat, 1);
			ItemMeta meta = item.getItemMeta();
			ArrayList<String> skinLoreList = new ArrayList<String>();
			meta.setDisplayName(dnaName);
			skinLoreList.add(dnaLore1);
			skinLoreList.add(dnaLore2);
			meta.setLore(skinLoreList);
			item.setItemMeta(meta);
			EnchantGlow.addGlow(item);
			return item;
		}
		
		public static Entity getTarget(final Player player) {
			 
	        BlockIterator iterator = new BlockIterator(player.getWorld(), player
	                .getLocation().toVector(), player.getEyeLocation()
	                .getDirection(), 0, 100);
	        Entity target = null;
	        while (iterator.hasNext()) {
	            Block item = iterator.next();
	            for (Entity entity : player.getNearbyEntities(100, 100, 100)) {
	                int acc = 2;
	                for (int x = -acc; x < acc; x++)
	                    for (int z = -acc; z < acc; z++)
	                        for (int y = -acc; y < acc; y++)
	                            if (entity.getLocation().getBlock()
	                                    .getRelative(x, y, z).equals(item)) {
	                                return target = entity;
	                            }
	            }
	        }
	        return target;
	    }
		
		@SuppressWarnings("unchecked")
		public String getPlayerDna(Player p){
			//System.out.println("Name = " + name);
			if(getSaves().getList("playerList." + p.getName()) != null){
				ArrayList<String> dnaTypesList = (ArrayList<String>) getSaves().getList("playerList." + p.getName());
				return dnaTypesList.toString();
			}else{
				return null;
			}
		}
		
		public AbilityUser userGet(Player player) {
		    for (AbilityUser user : abilityUserList) {
		        if (user.getPlayer.equals(player)){
		             return user;
		        }
		    }
		    return null;
		}
		
		public int findUserIndex(Player player) {
			int index = 0;
		    for (AbilityUser user : abilityUserList) {
		        if (user.getPlayer.equals(player)){
		             return index;
		        }else{
		        	index++;
		        }
		    }
		    return -1;
		}
		
		public machineBlock getMachineBlock(String name) {
		    for (machineBlock mb : machineBlockList) {
		        if (mb.getName.equals(name)){
		             return mb;
		        }
		    }
		    return null;
		}

		public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
	    	if ((cmd.getName().equals("advancedgenetics")) || (cmd.getName().equals("ag"))) {
	    		try{
		    		Player player = null;
		    		if (!(sender instanceof Player)) {
		    			if (args[0].equals("showAbilities")){
		    				sender.sendMessage("This command can only be run by a player!");
		    				return true;
		    			}
		    		}else
	    				player = (Player)sender;
		    		if ((args.length == 1) && (args[0].equals("reload"))) {
		    			reloadConfig();
		    			reloadSaves();
		    			sender.sendMessage("Config reloaded!");
		    			return true;
		    		}else if ((args.length == 1) && (args[0].equals("help"))) {
		    			throwError(sender);
		    			return true;
		    		}else if ((args.length == 3) && (args[0].equals("give"))) {
		    			Player dnaGetter = Bukkit.getPlayer(args[1]);
		    			if(dnaGetter != null){
		    				if(dnaTypesList.contains(args[2])){
		    					addDna(dnaGetter, player, args[2]);
		    				}else{
		    					sender.sendMessage("§c" + args[2] + " §cis not a valid DNA type!");
		    				}
		    			}else{
		    				sender.sendMessage("§c" + args[1] + " §ccan not be found!");
		    			}
		    			return true;
		    		}else if ((args.length == 1) && (args[0].equals("abilities"))) {
		    			sender.sendMessage("Advanced Genetics Abilities:");
		    			sender.sendMessage(dnaTypesList.toString());
		    			return true;
		    		}else if ((args.length == 2) && (args[0].equals("clearAbilities"))) {
		    			if(getServer().getPlayer(args[1]) != null){
		    				Player p = getServer().getPlayer(args[1]);
		    				removeAllAbilities(p);
		    			}else
		    				sender.sendMessage("§cPlayer " + args[1] + " §cnot found!");
		    		}else if ((args.length == 1) && (args[0].equals("showAbilities"))) {
		    			if (getTarget(player) != null){
			    			Entity targeted = getTarget(player);
			    			if (targeted instanceof Player){
			    				Player tPlayer = (Player) targeted;
								String name = tPlayer.getDisplayName();
								if(name.startsWith("§")){
								   name = name.substring(2);
								}
								if(name.contains("§")){
									name = name.substring(0, name.length()-2);
								}
								//System.out.println("playerList." + name);
				    			if(getPlayerDna(tPlayer) != null){
				    				String playerDna = getPlayerDna(tPlayer);
				    				player.sendMessage("§7" + name + "§7 has the genetic changes: " + playerDna);
				    			}else{
				    				player.sendMessage("§7" + name + " §7Has no genetic changes.");
				    			}
			    			}else{
			    				sender.sendMessage("§cTarget is not a player!");
			    			}
		    			}else{
		    				String name = player.getDisplayName();
							if(name.startsWith("§")){
								   name = name.substring(2);
								}
							if(name.contains("§")){
								name = name.substring(0, name.length()-2);
							}
		    				//System.out.println("playerList." + name);
			    			if(getPlayerDna(player) != null){
			    				String playerDna = getPlayerDna(player);
			    				player.sendMessage("§7" + name + "§7 has the genetic changes: " + playerDna);
			    			}else{
			    				player.sendMessage("§7" + name + " §7Has no genetic changes.");
			    			}
		    			}
		    			return true;
		    		}
		    	}catch(Exception x){}
	    	}
			sender.sendMessage("§cWrong arguments!");
			throwError(sender);
	    	return true; 
		}
	    
	    public void throwError(CommandSender sender) {
	    	sender.sendMessage("§6--Advanced Genetics v" + Bukkit.getServer().getPluginManager().getPlugin("AdvancedGenetics").getDescription().getVersion() + "--");
			sender.sendMessage("§eUsage: §f/ag reload");
			sender.sendMessage("§eUsage: §f/ag give <player> <ability>");
			sender.sendMessage("§eUsage: §f/ag abilities");
			sender.sendMessage("§eUsage: §f/ag clearAbilities <player>");
			sender.sendMessage("§eUsage: §f/ag showAbilities");
	    }
}

class AbilityUser {
	
	public Player getPlayer;
	public boolean getFireBallCooling;
	public boolean getMilkCooling;
	public boolean getTeleportCooling;
	public boolean getWoolCooling;
	public boolean getFlyCooling;
	public double getLastY;
	
	AbilityUser (Player abilityUsingPlayer) {
		getPlayer = abilityUsingPlayer;
		getFireBallCooling = false;
		getMilkCooling = false;
		getTeleportCooling = false;
		getWoolCooling = false;
		getFlyCooling = false;
		getLastY = 0;
	}
	
	public void setFire(boolean b){
		getFireBallCooling = b;
	}
	
	public void setMilk(boolean b){
		getMilkCooling = b;
	}
	
	public void setTeleport(boolean b){
		getTeleportCooling = b;
	}
	
	public void setWool(boolean b){
		getWoolCooling = b;
	}
	
	public void setFly(boolean b){
		getFlyCooling = b;
	}
	
	public void setUserY(int y){
		getLastY = y;
	}
	
	public String toString () {
		return "FireBallCooling: " + getFireBallCooling + " MilkCooling: " + getMilkCooling + " TeleportCooling: " + getTeleportCooling + "WoolCooling: " + getWoolCooling + "FlyCooling: " + getFlyCooling + "Y: " + getLastY;
	}
}

class machineBlock {
	
	public Location getLocation;
	public boolean getPariclesOn;
	public String getName;
	
	machineBlock (Location location, String name, boolean pariclesOn) {
		getLocation = location;
		getPariclesOn = pariclesOn;
		getName = name;
	}
	
	public String toString () {
		return "Location: " + getLocation + " Particles on: " + getPariclesOn;
	}
}